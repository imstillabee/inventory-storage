import mongoose, { Schema } from 'mongoose'
import Site from './Site'
import rackNameHelper from '../middleware/rackname'
import macMatcher from '../middleware/macMatcher'

const deviceSchema = new Schema({
  site: {
    type: String
  },
  ipAddress: {
    type: String
  },
  macAddress: {
    type: String,
    required: true,
    unique: true
  },
  collector: {
    type: String
  },
  inventoryInfo: {
    type: Object
  }
},
{
  timestamps: {
    createdAt: 'created_at'
  }
})

deviceSchema.post('findOneAndUpdate', rackNameHelper)
deviceSchema.post('findOneAndUpdate', macMatcher)

export default mongoose.model('Device', deviceSchema)
