import mongoose, { Schema } from 'mongoose'

const itemSchema = new Schema({
  macAddress: { type: String, required: true },
  ipAddress: {type: String },
  port: { type: String },
  slot: { type: String },
  description: { type: String },
  vlan: { type: String },
  role: { type: String }
},{
  id: false,
  _id: false,
  toObject: {
    virtuals: true
  },
  toJSON: {
    virtuals: true
  }
})

itemSchema.virtual('info', {
  ref: 'Device',
  localField: 'macAddress',
  foreignField: 'macAddress',
  justOne: true,
})

const rackSchema = new Schema({
  location: { type: String },
  name: { type: String },
  devices: [itemSchema]
})

const regionSchema = new Schema({
  region: {
    type: String,
    required: true
  },
  racks: [rackSchema]
})

const siteSchema = new Schema({
  name: { type: String, required: true, unique: true },
  type: { type: String },
  clli: { type: String },
  regions: [ regionSchema ]
},
{
  timestamps: {
    createdAt: 'created_at'
  }
})

export default mongoose.model('Site', siteSchema)
