import mongoose, { Schema } from 'mongoose'
import discoveryParser from '../middleware/discoveryParser'

const discoverySchema = new Schema({
  site: {
    type: String,
    required: true
  },
  clli: {
    type: String,
    required: true
  },
  discovery: {
    type: Array,
    required: true
  }
},
{
  timestamps: {
    createdAt: 'created_at'
  }
})

discoverySchema.post('findOneAndUpdate', discoveryParser)

export default mongoose.model('Discovery', discoverySchema)
