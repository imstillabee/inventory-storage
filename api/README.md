# What's here
| Folder | Description |
|:-------|:------------|
| controllers | logic for handling HTTP requests to the api's endpoints |
| middleware | middleware implemented in routes and in models, as well as helper functions for controller logic |
| models | schema definitions for Device and Site collections |
| routes | route definitions |
