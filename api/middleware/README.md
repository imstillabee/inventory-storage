# What's here
| Folder | Description |
|:-------|:------------|
| csvBuilders | The logic for creating CSV files for download |
| helpers | General helpers for finding information (ex: service tag) in the various stored devices |
| rackname | Middleware for Device model that updates rackname in Site model |
| siteParser | Middleware for POST request at /sites endpoint to convert JSON from collector into a valid site object |
| sitePush | Logic for building valid site objects to push to inventory api |
