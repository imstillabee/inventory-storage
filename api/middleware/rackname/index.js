import mongoose from 'mongoose'
import { Site } from '../../models'
import deviceSchema from '../../models/Device'

function bsRackname() {
  if (this._update.collector == 'bigswitch') {
    if(this._update.inventoryInfo.fabric_info) {
      let rackname = this._update.inventoryInfo.fabric_info.name
      if (rackname) {
        rackname = rackname.substr(7).split('-')
        rackname.pop()
        rackname = rackname.join('X')
        const macAdd = this._update.macAddress
        Site.findOne(
          {$or: [{name: this._update.site}, {clli: this._update.site}]},
          (err, result) => {
            if (err) throw err
            result.regions.forEach((region) => {
              region.racks.forEach((rack) => {
                rack.devices.forEach((device) => {
                  if (device.macAddress == macAdd) {
                    rack.name = rackname
                  }
                })
              })
            })
            Site.update({_id: result._id}, result, (err) => {
              if (err) {
                console.error(err)
              }
            })
          }
        )
      }
    }
  }
}

export default bsRackname
