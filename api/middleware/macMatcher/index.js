import mongoose from 'mongoose'
import { Site } from '../../models'
import deviceSchema from '../../models/Device'

function macMatcher() {
  if (this._update.collector == 'netapp') {
    const mac1 = this._update.macAddress
    const mac2 = this._update.inventoryInfo.controller2.mac
    Site.findOne({$or: [{name: this._update.site}, {clli: this._update.site}]},
      (err, result) => {
        if (err) throw err
        let found = false
        for (let region = 0; region < result.regions.length; region++) {
          for (let rack = 0; rack < result.regions[region].racks.length; rack++) {
            for (let device = 0; device < result.regions[region].racks[rack].devices.length; device++) {
              if (result.regions[region].racks[rack].devices[device].macAddress === mac1) {
                found = true
                break
              } else if (result.regions[region].racks[rack].devices[device].macAddress === mac2) {
                result.regions[region].racks[rack].devices[device].macAddress = mac1
                found = true
                break
              }
            }
            if (found) break
          }
          if (found) break
        }
        Site.update({_id: result._id}, result, (err) => {
          if (err) {
            console.error(err)
          }
        })
      }
    )
  }
}

export default macMatcher
