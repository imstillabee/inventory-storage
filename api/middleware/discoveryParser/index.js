import mongoose from 'mongoose'
import { Site, Device } from '../../models'
import {
  find_lite_site_router,
  find_core_site_router,
  find_lite_edge,
  find_core_edge,
  find_agg1,
  find_agg2,
  find_tor
} from './find_routers.js'
import { decideMacIp } from './helpers.js'

const rackname = new RegExp(/CR(?:BE|SW|ST)\w\w\w(\w+)-/)
const nodeRgx = /CR((SW)|(BS))/
const netappRgx = new RegExp(/netapp/i)

function discoveryParser(){
  const output = {
    name: this._update.site,
    clli: this._update.clli,
    regions: []
  }
  const data = this._update.discovery
  console.log(`\n\nNOW PARSING ${output.name}`)

  // no duplicate mac addresses
  const macAddSet = new Set()

  // Find lite site router by lldp
  const lite_site_router = find_lite_site_router(data)
  if (lite_site_router) {
    console.log(`\nFOUND LITE SITE ROUTER\n${lite_site_router.ip}\n`)

    // set up racks
    const rack = {
      location: output.clli,
      devices: []
    }
    const secRack = {
      location: output.clli,
      name: 'security',
      devices: []
    }

    // find edge router
    const lite_edge = find_lite_edge(data)
      if (lite_edge && lite_site_router.ip != lite_edge.ip) {
        console.log(`\nFOUND LITE EDGE ROUTER\n${lite_edge.ip}\n`)
        for (let item of lite_edge.irf) {
          const device = {
            macAddress: item['mac_address'].toLowerCase(),
            ipAddress: lite_edge.ip,
            slot: item.slot,
            description: lite_edge.lldp.local.global['sys_name'],
            role: 'Edge'
          }
          if (macAddSet.has(device.macAddress)) continue
          macAddSet.add(device.macAddress)
          rack.devices.push(device)
        }


      // Put Site Router devices in racks
      for (let item of lite_site_router.irf) {
        const device = {
          macAddress: item['mac_address'].toLowerCase(),
          ipAddress: lite_site_router.ip,
          slot: item.slot,
          description: lite_site_router.lldp.local.global['sys_name'],
          role: 'Site'
        }
        if (macAddSet.has(device.macAddress)) continue
        macAddSet.add(device.macAddress)
        if (device.slot == '1' || device.slot == '2') {
          rack.devices.push(device)
        } else {
          secRack.devices.push(device)
        }
      }

      // Find devices to put in racks
      const netapps = []
      const upLinks = lite_site_router.interfaces.filter(inter => inter.state == 'UP' && inter.interface.indexOf('GigabitEthernet') != -1)
      for (let item of upLinks) {
        const device = {description: item.description}
        if (device.description.indexOf('5900') != -1) continue
        if (/LEAF/.test(device.description)) device.role = 'Leaf'
        if (/SPINE/.test(device.description)) device.role = 'Spine'
        if (/BSFD/.test(device.description)) device.role = 'Bigswitch'
        const lookup = item.interface.replace('GigabitEthernet', 'GE')
        device.slot = lookup.replace('GE', '').split('/')[0]
        device.port = lookup.replace('GE', '').split('/')[2]
        const macList = lite_site_router.arp.filter(mac => mac['Interface'] == lookup)
        if (netappRgx.test(device.description)) {
          device.macAddress = macList
          netapps.push(device)
          continue
        } else if (/^[12]$/.test(device.slot) && /^16$/.test(device.port)) {
          // edge case for atlanta
          device.macAddress = macList
          netapps.push(device)
          continue
        }
        if (macList.length < 1) continue
        if (macList.length == 1) {
          device.ipAddress = macList[0]['IP address']
          device.macAddress = macList[0]['MAC address']
        } else {
          const macIp = decideMacIp(item, macList)
          device.ipAddress = macIp.ipAddress
          device.macAddress = macIp.macAddress
        }
        // name the racks if possible
        if (rackname.test(device.description)) {
          device.slot === '3'
            ? secRack.name = rackname.exec(device.description)[1]
            : rack.name = rackname.exec(device.description)[1]
        }
        if (macAddSet.has(device.macAddress)) continue
        macAddSet.add(device.macAddress)
        if (device.slot == '1' || device.slot == '2') {
          rack.devices.push(device)
        } else {
          secRack.devices.push(device)
        }
      }
      // deal with netapps
      if (netapps.length > 0) {
        var netapp_device = netapps[0]
        for (let netapp of netapps) {
          if (netapp.macAddress.length > netapp_device.macAddress.length) {
            netapp_device = netapp
          }
        }
        netapp_device.ipAddress = netapp_device.macAddress.filter(item => item['IP address'].split('.')[3] == '240')[0]['IP address']
        netapp_device.macAddress = netapp_device.macAddress.filter(item => item['IP address'].split('.')[3] == '240')[0]['MAC address']
        netapp_device.role = 'Netapp'
        if (!macAddSet.has(netapp_device.macAddress)) {
          macAddSet.add(netapp_device.macAddress)
          rack.devices.push(netapp_device)
        }
      }
      // add in termserver
      const termList = lite_site_router.lldp.neighbors.filter(item => item['sys_description'].indexOf('Cisco') != -1)
      if (termList.length > 0) {
        const device = {
          macAddress: termList[0]['chassis_id'],
          ipAddress: termList[0]['mgmt_addr'],
          port: termList[0]['interface'].split('/')[2],
          slot: termList[0]['interface'].split('/')[0],
          description: termList[0]['sys_description'],
          role: 'Termserver'
        }
        if (!macAddSet.has(device.macAddress)) {
          macAddSet.add(device.macAddress)
          rack.devices.push(device)
        }
      }
      const region = {
        region: 'r1',
        racks: [rack, secRack]
      }
      output.regions.push(region)
    }
  }

  // Find core site router by lldp
  const coreSiteRouter = find_core_site_router(data)
  if (coreSiteRouter) {
    console.log(`\nFOUND CORE SITE ROUTER\n${coreSiteRouter.ip}`)

    const coreEdge = find_core_edge(data)
    if (coreEdge) console.log(`\nFOUND CORE EDGE ROUTER\n${coreEdge.ip}`)

    const edgeAgg = find_tor(coreEdge, data, '10')
    if (edgeAgg) console.log(`\nFOUND CORE edgeAgg\n${edgeAgg.ip}`)

    const agg1 = find_agg1(data)
    const agg2 = find_agg2(data)

    // Handle shared network cabinets
    if (agg1 && !agg2) {
      // Pod 1 only
      console.log(`\n\nFOUND CORE AGG1\n${agg1.ip}`)

      // find pod1 tors
      const torList = []
      const r1Tor = find_tor(agg1, data, '1', '5')
      if (r1Tor) {
        torList.push({region: 'r1', tor: r1Tor})
        console.log(`\nFOUND r1Tor\n${r1Tor.ip}`)
      }
      const r2Tor = find_tor(agg1, data, '2', '37')
      if (r2Tor) {
        torList.push({region: 'r2', tor: r2Tor})
        console.log(`\nFOUND r2Tor\n${r2Tor.ip}`)
      }
      const r3Tor = find_tor(agg1, data, '3', '69')
      if (r3Tor) {
        torList.push({region: 'r3', tor: r3Tor})
        console.log(`\nFOUND r3Tor\n${r3Tor.ip}`)
      }
      const r4Tor = find_tor(agg1, data, '4', '101')
      if (r4Tor) {
        torList.push({region: 'r4', tor: r4Tor})
        console.log(`\nFOUND r4Tor\n${r4Tor.ip}`)
      }
      const secTor = find_tor(agg1, data, '5')
      if (secTor) console.log(`\nFOUND secTor\n${secTor.ip}`)
      const monTor = find_tor(agg1, data, '6')
      if (monTor) console.log(`\nFOUND monTor\n${monTor.ip}`)
      const monFE = find_tor(agg1, data, '7')
      if (monFE) console.log(`\nFOUND monFE\n${monFE.ip}`)


      const shared = {region: 'shared', racks: []}
      // rack13 holds devices from slots 1 and 3
      const rack13 = {
        location: output.clli,
        devices: []
      }
      // rack24 holds devices from slots 2 and 4
      const rack24 = {
        location: output.clli,
        devices: []
      }

      // put in devices for site router
      for (let item of coreSiteRouter.irf) {
        const device = {
          macAddress: item['mac_address'].toLowerCase(),
          ipAddress: coreSiteRouter.ip,
          slot: item.slot,
          description: coreSiteRouter.lldp.local.global['sys_name'],
          role: 'Site'
        }
        if (macAddSet.has(device.macAddress)) continue
        macAddSet.add(device.macAddress)
        if (device.slot == 1 || device.slot == 3) {
          rack13.devices.push(device)
        } else {
          rack24.devices.push(device)
        }
      }

      // put in agg1
      for (let item of agg1.irf) {
        const device = {
          macAddress: item['mac_address'].toLowerCase(),
          ipAddress: agg1.ip,
          slot: item.slot,
          description: agg1.lldp.local.global['sys_name'],
          role: 'Agg'
        }
        if (macAddSet.has(device.macAddress)) continue
        macAddSet.add(device.macAddress)
        if (device.slot == 1 || device.slot == 3) {
          rack13.devices.push(device)
        } else {
          rack24.devices.push(device)
        }
      }

      // put in edge router
      if (coreEdge) {
        for (let item of coreEdge.irf) {
          const device = {
            macAddress: item['mac_address'].toLowerCase(),
            ipAddress: coreEdge.ip,
            slot: item.slot,
            description: coreEdge.lldp.local.global['sys_name'],
            role: 'Edge'
          }
          if (macAddSet.has(device.macAddress)) continue
          macAddSet.add(device.macAddress)
          if (device.slot == 1) {
            rack13.devices.push(device)
          } else {
            rack24.devices.push(device)
          }
        }
      }

      // add in Cisco devices
      const ciscoDevices = coreSiteRouter.lldp.neighbors.filter(nbor => {
        return (
          nbor['sys_description'].indexOf('Cisco') != -1 &&
          nbor.port == 'Gi0/0'
        )
      })
      for (let item of ciscoDevices) {
        const device = {
          macAddress: item['chassis_id'],
          ipAddress: item['mgmt_addr'],
          slot: item.interface.split('/')[0],
          port: item.interface.split('/')[3],
          description: item.sysname.replace('.vzw.com', ''),
          role: 'Termserver'
        }
        if (macAddSet.has(device.macAddress)) continue
        macAddSet.add(device.macAddress)
        if (device.slot == 1 || device.slot == 3) {
          rack13.devices.push(device)
        } else {
          rack24.devices.push(device)
        }
      }

      // grab leaves
      const agg1Leaves = agg1.lldp.neighbors.filter(nbor => nodeRgx.test(nbor.sysname) && nbor['int_type'] == 'FortyGigE')
      for (let item of agg1Leaves) {
        const device = {
          macAddress: item['chassis_id'],
          slot: item.interface.split('/')[0],
          port: item.interface.split('/')[3],
          description: item.sysname,
          role: 'Leaf'
        }
        const ipList = coreSiteRouter.arp.filter(entry => entry['MAC address'] == item['chassis_id'])
        if (ipList.length == 1) {
          device.ipAddress = ipList[0]['IP address']
        } else if (ipList.length > 1) {
          device.ipAddress = decideMacIp(ipList).ipAddress
        }
        if (macAddSet.has(device.macAddress)) continue
        macAddSet.add(device.macAddress)
        if (device.slot == 1 || device.slot == 3) {
          rack13.devices.push(device)
        } else {
          rack24.devices.push(device)
        }
      }

      // find bigswitch controllers
      const findControls = coreSiteRouter.arp.filter(entry => (
        entry['MAC address'].indexOf('5c16-') == 0 ||
        entry['MAC address'].indexOf('1866-') == 0
      ) && entry['Interface'].indexOf('XGE') == 0)
      const bsControls = []
      const bsSet = new Set()
      for (let ctl of findControls) {
        if (bsSet.has(ctl['MAC address'])) continue
        if (/19|21|23/.test(ctl['VLAN'])) continue
        const dups = findControls.filter(item => item['MAC address'] == ctl['MAC address'])
        const bsCtl = dups.sort((a,b) => parseInt(b['IP address'].split('.')[3]) - parseInt(a['IP address'].split('.')[3]))[0]
        bsSet.add(bsCtl['MAC address'])
        bsControls.push(bsCtl)
      }
      for (let ctl of bsControls) {
        const device = {
          macAddress: ctl['MAC address'],
          ipAddress: ctl['IP address'],
          slot: ctl['Interface'].replace('XGE','').split('/')[0],
          port: ctl['Interface'].replace('XGE','').split('/')[2],
          role: 'Bigswitch'
        }
        const findDesc = coreSiteRouter.interfaces.filter(item => item.interface == ctl['Interface'].replace('XGE','GigabitEthernet'))
        if (findDesc.length == 1) device.description = findDesc[0].description
        // NOTE: This will probably not put them in the correct rack.
        //   However, there appears to be no pattern we can rely on for that.
        if (macAddSet.has(device.macAddress)) continue
        macAddSet.add(device.macAddress)
        if (device.slot == 1 || device.slot == 2) {
          rack13.devices.push(device)
        } else {
          rack24.devices.push(device)
        }
      }

      // get agg1 spines
      const agg1Spines = agg1['mac_adds'].filter(
        entry => /12|14|16|18/.test(entry['VLAN_ID']) &&
        entry['Port/NickName'].indexOf('XGE') != -1 &&
        coreSiteRouter.arp.filter(item => item['MAC address'] == entry['MAC_Address']).length > 0 &&
        !macAddSet.has(entry['MAC_Address']) &&
        parseInt(entry['Port/NickName'].split('/')[2]) > 20
      )
      for (let item of agg1Spines) {
        const device = {
          macAddress: item['MAC_Address'],
          slot: item['Port/NickName'].replace('XGE', '').split('/')[0],
          port: item['Port/NickName'].replace('XGE', '').split('/')[2],
          vlan: item['VLAN_ID'],
          role: 'Spine'
        }
        const ip = coreSiteRouter.arp.filter(entry => entry['MAC address'] == item['MAC_Address'])
        if (ip.length > 0) {
          device.ipAddress = ip[0]['IP address']
        }
        if (macAddSet.has(device.macAddress)) continue
        macAddSet.add(device.macAddress)
        if (device.slot == 1 || device.slot == 3) {
          rack13.devices.push(device)
        } else {
          rack24.devices.push(device)
        }
      }

      // name the racks
      for (let device of rack13.devices) {
        if (rackname.test(device.description)) {
          rack13.name = rackname.exec(device.description)[1]
          break
        }
      }
      for (let device of rack24.devices) {
        if (rackname.test(device.description)) {
          rack24.name = rackname.exec(device.description)[1]
          break
        }
      }

      // fill shared region and push to output
      shared.racks.push(rack13)
      shared.racks.push(rack24)
      output.regions.push(shared)


      // Parse the regular tors
      for (let tor of torList) {
        // set up data structures
        const region = {region: tor.region, racks: []}
        const rack1 = {location: output.clli, devices: []}
        const rack2 = {location: output.clli, devices: []}
        const rack3 = {location: output.clli, devices: []}
        const rack4 = {location: output.clli, devices: []}

        // split the tor irf into the racks
        for (let item of tor.tor.irf) {
          const device = {
            macAddress: item['mac_address'].toLowerCase(),
            ipAddress: tor.tor.ip,
            slot: item.slot,
            description: tor.tor.lldp.local.global['sys_name'],
            role: 'TOR'
          }
          if (macAddSet.has(device.macAddress)) continue
          macAddSet.add(device.macAddress)
          if (device.slot == 1) {
            rack1.devices.push(device)
          } else if (device.slot == 2) {
            rack2.devices.push(device)
          } else if (device.slot == 3) {
            rack3.devices.push(device)
          } else if (device.slot == 4) {
            rack4.devices.push(device)
          }
        }

        // Get devices to put in racks
        const linkList = tor.tor.lldp.local.local.filter(
          item => item.name.indexOf('Gigabit') == -1 &&
          item.type == 'GigabitEthernet'
        )
        for (let link of linkList) {
          const device = {
            description: link.name,
            slot: link.interface.split('/')[0],
            port: link.interface.split('/')[2]
          }
          const lookup = 'GE' + link.interface
          const macList = tor.tor['mac_adds'].filter(mac => mac['Port/NickName'] == lookup)
          const ipList = macList.map(mac => coreSiteRouter.arp.filter(
            entry => entry['MAC address'] == mac['MAC_Address'])
          )
          if (macList.length == 0) {
            console.log(`\nSkipping - Did not find a mac for ${region.region} ${lookup} ${device.description}`)
            continue
          } else if (macList.length == 1) {
            device.macAddress = macList[0]['MAC_Address']
            if (ipList[0].length == 0) {
              console.log(`\nDid not find an ip for ${macList[0]['MAC_Address']} ${region.region} ${lookup} ${device.description}`)
            }
            if (ipList[0].length == 1) {
              device.ipAddress = ipList[0][0]['IP address']
            } else {
              console.log(`\nSingle mac, multiple ip: ${region.region} ${lookup} ${device.macAddress}`)
            }
          } else {
            const checkIp = ipList.filter(item => item.length > 0)
            if (checkIp.length == 0) {
              console.log(`\nSkipping - no ip for ${region.region} ${lookup} ${device.description}`)
              continue
            } else if (checkIp.length == 1 && checkIp[0].length == 1) {
              device.macAddress = checkIp[0][0]['MAC address']
              device.ipAddress = checkIp[0][0]['IP address']
            } else {
              for (let arr of checkIp) {
                if (arr.length > 1) console.log(`\n\n MULTPLE IPS FOUND ${region.region} ${lookup} ${device.description}`)
              }
              const highestOctet = checkIp.sort((a, b) => parseInt(b[0]['IP address'].split('.')[3]) - parseInt(a[0]['IP address'].split('.')[3]))
              device.macAddress = highestOctet[0][0]['MAC address']
              device.ipAddress = highestOctet[0][0]['IP address']
            }
          }
          if (macAddSet.has(device.macAddress)) continue
          macAddSet.add(device.macAddress)
          if (device.slot == 1) {
            rack1.devices.push(device)
          } else if (device.slot == 2) {
            rack2.devices.push(device)
          } else if (device.slot == 3) {
            rack3.devices.push(device)
          } else if (device.slot == 4) {
            rack4.devices.push(device)
          }
        }

        // gather Storage
        const storageDevices = tor.tor.arp.filter(
          entry => /^GE[1-4]\/0\/[3-4]$/.test(entry['Interface']))
        for (let item of storageDevices) {
          const device = {
            macAddress: item['MAC address'],
            ipAddress: item['IP address'],
            slot: item['Interface'].replace('GE', '').split('/')[0],
            port: item['Interface'].replace('GE', '').split('/')[2],
            role: 'Storage'
          }
          if (macAddSet.has(device.macAddress)) continue
          macAddSet.add(device.macAddress)
          if (device.slot == 1) {
            rack1.devices.push(device)
          } else if (device.slot == 2) {
            rack2.devices.push(device)
          } else if (device.slot == 3) {
            rack3.devices.push(device)
          } else if (device.slot == 4) {
            rack4.devices.push(device)
          }
        }

        // get the bigswitch leaves
        const bsLeaves = tor.tor.mac_adds.filter(
          mac => /^GE[1-4]\/0\/3[2-9]/.test(mac['Port/NickName']))
        for (let item of bsLeaves) {
          const device = {
            macAddress: item['MAC_Address'],
            slot: item['Port/NickName'].replace('GE', '').split('/')[0],
            port: item['Port/NickName'].replace('GE', '').split('/')[2],
            role: 'Leaf',
            vlan: item['VLAN_ID']
          }
          const ipList = coreSiteRouter.arp.filter(
            entry => entry['MAC address'] == device.macAddress
          )
          if (ipList.length == 0) {
            console.log(`\nNo IP found for leaf ${region.region} ${device.macAddress}`)
          } else if (ipList.length == 1) {
            device.ipAddress = ipList[0]['IP address']
          } else {
            console.log(`\nMultiple IP found for leaf ${region.region} ${device.macAddress}\n${ipList}`)
          }
          if (macAddSet.has(device.macAddress)) continue
          macAddSet.add(device.macAddress)
          if (device.slot == 1) {
            rack1.devices.push(device)
          } else if (device.slot == 2) {
            rack2.devices.push(device)
          } else if (device.slot == 3) {
            rack3.devices.push(device)
          } else if (device.slot == 4) {
            rack4.devices.push(device)
          }
        }

        // name the racks
        for (let device of rack1.devices) {
          if (rackname.test(device.description)) {
            rack1.name = rackname.exec(device.description)[1]
            break
          }
        }
        for (let device of rack2.devices) {
          if (rackname.test(device.description)) {
            rack2.name = rackname.exec(device.description)[1]
            break
          }
        }
        for (let device of rack3.devices) {
          if (rackname.test(device.description)) {
            rack3.name = rackname.exec(device.description)[1]
            break
          }
        }
        for (let device of rack4.devices) {
          if (rackname.test(device.description)) {
            rack4.name = rackname.exec(device.description)[1]
            break
          }
        }

        // fill region and push to output
        region.racks.push(rack1)
        region.racks.push(rack2)
        region.racks.push(rack3)
        region.racks.push(rack4)
        output.regions.push(region)
      }

      // Get info for monitoring racks
      if (monTor) {
        const region = {region: 'm1', racks:[]}
        const rack = {location: output.clli, devices:[]}

        // put the tor in
        for (let item of monTor.irf) {
          const device = {
            macAddress: item['mac_address'].toLowerCase(),
            ipAddress: monTor.ip,
            slot: item.slot,
            description: monTor.lldp.local.global['sys_name'],
            role: 'TOR'
          }
          if (macAddSet.has(device.macAddress)) continue
          macAddSet.add(device.macAddress)
          rack.devices.push(device)
        }

        // Add in monitoring edge
        if (monFE) {
          for (let item of monFE.irf) {
            const device = {
              macAddress: item['mac_address'].toLowerCase(),
              ipAddress: monTor.ip,
              slot: item.slot,
              description: monTor.lldp.local.global['sys_name'],
            }
            if (macAddSet.has(device.macAddress)) continue
            macAddSet.add(device.macAddress)
            rack.devices.push(device)
          }
        }

        // Get other devices
        const upLinks = monTor.interfaces.filter(
          inter => inter.state == 'UP' && /^GigabitEthernet[1-9]/.test(inter.interface)
        )
        for (let link of upLinks) {
          const device = {
            description: link.description,
            slot: link.interface.replace('GigabitEthernet', '').split('/')[0],
            port: link.interface.replace('GigabitEthernet', '').split('/')[2],
          }
          const macList = monTor.mac_adds.filter(
            item => item['Port/NickName'] == link.interface.replace('GigabitEthernet','GE')
          )
          if (macList.length == 0) continue
          if (macList.length == 1) {
            device.macAddress = macList[0]['MAC_Address']
            device.vlan = macList[0]['VLAN_ID']
          } else if (macList.length > 1) {
            // if one of the mac addresses begins with 847b, use it - that's hypermon
            const hypermonMac = macList.filter(mac => mac['MAC_Address'].indexOf('847b') == 0)
            if (hypermonMac.length == 1) {
              device.macAddress = hypermonMac[0]['MAC_Address']
              device.vlan = hypermonMac[0]['VLAN_ID']
            } else {
              console.log(`\nCould not find hypermon mac: ${device.description} ${link.interface}`)
            }
          }
          if (!device.macAddress) continue
          const ipList = coreSiteRouter.arp.filter(entry => entry['MAC address'] == device.macAddress)
          if (ipList.length == 0) {
            console.log(`\nCould not find ip address for m1 ${device.macAddress} ${device.description}`)
          } else if (ipList.length == 1) {
            device.ipAddress = ipList[0]['IP address']
          } else {
            console.log(`\nMultiple IPs for m1 ${device.macAddress} ${device.description}`)
          }
          if (macAddSet.has(device.macAddress)) continue
          macAddSet.add(device.macAddress)
          rack.devices.push(device)
        }

        // name the rack
        for (let device of rack.devices) {
          if (rackname.test(device.description)) {
            rack.name = rackname.exec(device.description)[1]
            break
          }
        }

        region.racks.push(rack)
        output.regions.push(region)
      }

      // Add security and EMC stacks
      if (secTor) {
        const region = {region: 'utility', racks:[]}
        const rack1 = {location: output.clli, name: 'storage', devices: []}
        const rack2 = {location: output.clli, name: 'security', devices: []}
        const rack3 = {location: output.clli, name: 'security', devices: []}
        const rack4 = {location: output.clli, name: 'storage', devices: []}

        // put the tors in
        for (let item of secTor.irf) {
          const device = {
            macAddress: item['mac_address'].toLowerCase(),
            ipAddress: secTor.ip,
            slot: item.slot,
            description: secTor.lldp.local.global['sys_name'],
            role: 'TOR'
          }
          if (macAddSet.has(device.macAddress)) continue
          macAddSet.add(device.macAddress)
          if (device.slot == 1) {
            rack1.devices.push(device)
          } else if (device.slot == 2) {
            rack2.devices.push(device)
          } else if (device.slot == 3) {
            rack3.devices.push(device)
          } else if (device.slot == 4) {
            rack4.devices.push(device)
          }
        }

        // find other devices
        const devices = secTor['mac_adds'].filter(
          mac => /^GE[1-9]/.test(mac['Port/NickName']))
        for (let item of devices) {
          const description = secTor.interfaces.filter(
            inter => inter.interface == item['Port/NickName'].replace('GE', 'GigabitEthernet')
          )[0].description
          const device = {
            macAddress: item['MAC_Address'],
            description: description,
            slot: item['Port/NickName'].replace('GE', '').split('/')[0],
            port: item['Port/NickName'].replace('GE', '').split('/')[2],
            vlan: item['VLAN_ID']
          }
          const ipList = coreSiteRouter.arp.filter(
            entry => entry['MAC address'] == item['MAC_Address']
          )
          if (ipList.length == 0) {
            console.log(`\nNo ip found for utility ${item['MAC_Address']} ${item['Port/NickName']}`)
          } else if (ipList.length == 1) {
            device.ipAddress = ipList[0]['IP address']
          } else {
            device.ipAddress = ipList.sort(
              (a,b) => parseInt(a['IP address'].split('.')[3]) - parseInt(b['IP address'].split('.')[3])
            )[0]['IP address']
          }
          if (macAddSet.has(device.macAddress)) continue
          macAddSet.add(device.macAddress)
          if (device.slot == 1) {
            rack1.devices.push(device)
          } else if (device.slot == 2) {
            rack2.devices.push(device)
          } else if (device.slot == 3) {
            rack3.devices.push(device)
          } else if (device.slot == 4) {
            rack4.devices.push(device)
          }
        }

        // name the racks
        for (let device of rack1.devices) {
          if (rackname.test(device.description)) {
            rack1.name = rackname.exec(device.description)[1]
            break
          }
        }
        for (let device of rack2.devices) {
          if (rackname.test(device.description)) {
            rack2.name = rackname.exec(device.description)[1]
            break
          }
        }
        for (let device of rack3.devices) {
          if (rackname.test(device.description)) {
            rack3.name = rackname.exec(device.description)[1]
            break
          }
        }
        for (let device of rack4.devices) {
          if (rackname.test(device.description)) {
            rack4.name = rackname.exec(device.description)[1]
            break
          }
        }

        // fill region and push to output
        region.racks.push(rack1)
        region.racks.push(rack2)
        region.racks.push(rack3)
        region.racks.push(rack4)
        output.regions.push(region)
      }

    } else if (agg1 && agg2) {
      // Pods 1 and 2
      console.log(`\n\nFOUND CORE AGG1\n${agg1.ip}`)

      // find pod1 tors
      const torList = []
      const r1Tor = find_tor(agg1, data, '1', '5')
      if (r1Tor) {
        torList.push({region: 'r1', tor: r1Tor})
        console.log(`\nFOUND r1Tor\n${r1Tor.ip}`)
      }
      const r2Tor = find_tor(agg1, data, '2', '37')
      if (r2Tor) {
        torList.push({region: 'r2', tor: r2Tor})
        console.log(`\nFOUND r2Tor\n${r2Tor.ip}`)
      }
      const r3Tor = find_tor(agg1, data, '3', '69')
      if (r3Tor) {
        torList.push({region: 'r3', tor: r3Tor})
        console.log(`\nFOUND r3Tor\n${r3Tor.ip}`)
      }
      const r4Tor = find_tor(agg1, data, '4', '101')
      if (r4Tor) {
        torList.push({region: 'r4', tor: r4Tor})
        console.log(`\nFOUND r4Tor\n${r4Tor.ip}`)
      }
      const secTor = find_tor(agg1, data, '5')
      if (secTor) console.log(`\nFOUND secTor\n${secTor.ip}`)
      const monTor = find_tor(agg1, data, '6')
      if (monTor) console.log(`\nFOUND monTor\n${monTor.ip}`)
      const monFE = find_tor(agg1, data, '7')
      if (monFE) console.log(`\nFOUND monFE\n${monFE.ip}`)

      // find pod2 tors
      console.log(`\n\nFOUND CORE AGG2\n${agg2.ip}`)
      const r5Tor = find_tor(agg2, data, '1')
      if (r5Tor) {
        torList.push({region: 'r5', tor: r5Tor})
        console.log(`\nFOUND r5Tor\n${r5Tor.ip}`)
      }
      const r6Tor = find_tor(agg2, data, '2', '165')
      if (r6Tor) {
        torList.push({region: 'r6', tor: r6Tor})
        console.log(`\nFOUND r6Tor\n${r6Tor.ip}`)
      }
      const r7Tor = find_tor(agg2, data, '3', '197')
      if (r7Tor) {
        torList.push({region: 'r7', tor: r7Tor})
        console.log(`\nFOUND r7Tor\n${r7Tor.ip}`)
      }
      const r8Tor = find_tor(agg2, data, '4', '229')
      if (r8Tor) {
        torList.push({region: 'r8', tor: r8Tor})
        console.log(`\nFOUND r8Tor\n${r8Tor.ip}`)
      }

      // set up data structure
      const shared = {region: 'shared', racks: []}
      // rack13 holds devices from site router slot 1 and agg1 slots 1 and 3
      const rack13 = {
        location: output.clli,
        devices: []
      }
      // rack24 holds devices from site router slot 2 and agg1 slots 2 and 4
      const rack24 = {
        location: output.clli,
        devices: []
      }
      // rack57 holds devices from site router slot 3 and agg2 slots 1 and 3
      const rack57 = {
        location: output.clli,
        devices: []
      }
      // rack68 holds devices from site router slot 4 and agg2 slots 2 and 4
      const rack68 = {
        location: output.clli,
        devices: []
      }

      // put in devices for site router
      for (let item of coreSiteRouter.irf) {
        const device = {
          macAddress: item['mac_address'].toLowerCase(),
          ipAddress: coreSiteRouter.ip,
          slot: item.slot,
          description: coreSiteRouter.lldp.local.global['sys_name'],
          role: 'Site'
        }
        if (macAddSet.has(device.macAddress)) continue
        macAddSet.add(device.macAddress)
        if (device.slot == 1) {
          rack13.devices.push(device)
        } else if (device.slot == 2) {
          rack24.devices.push(device)
        } else if (device.slot == 3) {
          rack57.devices.push(device)
        } else if (device.slot == 4) {
          rack68.devices.push(device)
        }
      }

      // put in agg1
      for (let item of agg1.irf) {
        const device = {
          macAddress: item['mac_address'].toLowerCase(),
          ipAddress: agg1.ip,
          slot: item.slot,
          description: agg1.lldp.local.global['sys_name'],
          role: 'Agg'
        }
        if (macAddSet.has(device.macAddress)) continue
        macAddSet.add(device.macAddress)
        if (device.slot == 1 || device.slot == 3) {
          rack13.devices.push(device)
        } else {
          rack24.devices.push(device)
        }
      }

      // put in agg2
      for (let item of agg2.irf) {
        const device = {
          macAddress: item['mac_address'].toLowerCase(),
          ipAddress: agg2.ip,
          slot: item.slot,
          description: agg2.lldp.local.global['sys_name'],
          role: 'Agg'
        }
        if (macAddSet.has(device.macAddress)) continue
        macAddSet.add(device.macAddress)
        if (device.slot == 1 || device.slot == 3) {
          rack57.devices.push(device)
        } else {
          rack68.devices.push(device)
        }
      }

      // put in edge router
      if (coreEdge) {
        for (let item of coreEdge.irf) {
          const device = {
            macAddress: item['mac_address'].toLowerCase(),
            ipAddress: coreEdge.ip,
            slot: item.slot,
            description: coreEdge.lldp.local.global['sys_name'],
            role: 'Edge'
          }
          if (macAddSet.has(device.macAddress)) continue
          macAddSet.add(device.macAddress)
          if (device.slot == 1) {
            rack13.devices.push(device)
          } else {
            rack24.devices.push(device)
          }
        }
      }

      // put in edgeAgg
      if (edgeAgg) {
        for (let item of edgeAgg.irf) {
          const device = {
            macAddress: item['mac_address'].toLowerCase(),
            ipAddress: edgeAgg.ip,
            slot: item.slot,
            description: edgeAgg.lldp.local.global['sys_name'],
            role: 'Edge'
          }
          if (macAddSet.has(device.macAddress)) continue
          macAddSet.add(device.macAddress)
          if (device.slot == 1) {
            rack57.devices.push(device)
          } else {
            rack68.devices.push(device)
          }
        }
      }

      // add in Cisco devices
      const ciscoDevices = coreSiteRouter.lldp.neighbors.filter(nbor => {
        return (
          nbor['sys_description'].indexOf('Cisco') != -1 &&
          nbor.port == 'Gi0/0'
        )
      })
      for (let item of ciscoDevices) {
        const device = {
          macAddress: item['chassis_id'],
          ipAddress: item['mgmt_addr'],
          slot: item.interface.split('/')[0],
          port: item.interface.split('/')[3],
          description: item.sysname.replace('.vzw.com', ''),
          role: 'Termserver'
        }
        if (macAddSet.has(device.macAddress)) continue
        macAddSet.add(device.macAddress)
        if (device.slot == 1) {
          rack13.devices.push(device)
        } else if (device.slot == 2) {
          rack24.devices.push(device)
        } else if (device.slot == 3) {
          rack57.devices.push(device)
        } else if (device.slot == 4) {
          rack68.devices.push(device)
        }
      }

      // grab leaves
      const agg1Leaves = agg1.lldp.neighbors.filter(nbor => nodeRgx.test(nbor.sysname) && nbor['int_type'] == 'FortyGigE')
      const agg2Leaves = agg2.lldp.neighbors.filter(nbor => nodeRgx.test(nbor.sysname) && nbor['int_type'] == 'FortyGigE')
      for (let item of agg1Leaves) {
        const device = {
          macAddress: item['chassis_id'],
          slot: item.interface.split('/')[0],
          port: item.interface.split('/')[3],
          description: item.sysname,
          role: 'Leaf'
        }
        const ipList = coreSiteRouter.arp.filter(entry => entry['MAC address'] == item['chassis_id'])
        if (ipList.length == 1) {
          device.ipAddress = ipList[0]['IP address']
        } else if (ipList.length > 1) {
          device.ipAddress = decideMacIp(ipList).ipAddress
        }
        if (macAddSet.has(device.macAddress)) continue
        macAddSet.add(device.macAddress)
        if (device.slot == 1 || device.slot == 3) {
          rack13.devices.push(device)
        } else {
          rack24.devices.push(device)
        }
      }
      for (let item of agg2Leaves) {
        const device = {
          macAddress: item['chassis_id'],
          slot: item.interface.split('/')[0],
          port: item.interface.split('/')[3],
          description: item.sysname,
          role: 'Leaf'
        }
        const ipList = coreSiteRouter.arp.filter(entry => entry['MAC address'] == item['chassis_id'])
        if (ipList.length == 1) {
          device.ipAddress = ipList[0]['IP address']
        } else if (ipList.length > 1) {
          device.ipAddress = decideMacIp(ipList).ipAddress
        }
        if (macAddSet.has(device.macAddress)) continue
        macAddSet.add(device.macAddress)
        if (device.slot == 1 || device.slot == 3) {
          rack57.devices.push(device)
        } else {
          rack68.devices.push(device)
        }
      }

      // find bigswitch controllers
      const findControls = coreSiteRouter.arp.filter(entry => (
        entry['MAC address'].indexOf('5c16-') == 0 ||
        entry['MAC address'].indexOf('1866-') == 0
      ) && entry['Interface'].indexOf('XGE') == 0)
      const bsControls = []
      const bsSet = new Set()
      for (let ctl of findControls) {
        if (bsSet.has(ctl['MAC address'])) continue
        if (/19|21|23/.test(ctl['VLAN'])) continue
        const dups = findControls.filter(item => item['MAC address'] == ctl['MAC address'])
        const bsCtl = dups.sort((a,b) => parseInt(b['IP address'].split('.')[3]) - parseInt(a['IP address'].split('.')[3]))[0]
        bsSet.add(bsCtl['MAC address'])
        bsControls.push(bsCtl)
      }
      for (let ctl of bsControls) {
        const device = {
          macAddress: ctl['MAC address'],
          ipAddress: ctl['IP address'],
          slot: ctl['Interface'].replace('XGE','').split('/')[0],
          port: ctl['Interface'].replace('XGE','').split('/')[2],
          role: 'Bigswitch'
        }
        const findDesc = coreSiteRouter.interfaces.filter(item => item.interface == ctl['Interface'].replace('XGE','GigabitEthernet'))
        if (findDesc.length == 1) device.description = findDesc[0].description
        // NOTE: This will probably not put them in the correct rack.
        //   However, there appears to be no pattern we can rely on for that.
        if (macAddSet.has(device.macAddress)) continue
        macAddSet.add(device.macAddress)
        if (device.slot == 1) {
          rack13.devices.push(device)
        } else if (device.slot == 2) {
          rack24.devices.push(device)
        } else if (device.slot == 3) {
          rack57.devices.push(device)
        } else if (device.slot == 4) {
          rack68.devices.push(device)
        }
      }

      // get agg1 spines
      const agg1Spines = agg1['mac_adds'].filter(
        entry => /12|14|16|18/.test(entry['VLAN_ID']) &&
        entry['Port/NickName'].indexOf('XGE') != -1 &&
        coreSiteRouter.arp.filter(item => item['MAC address'] == entry['MAC_Address']).length > 0 &&
        !macAddSet.has(entry['MAC_Address']) &&
        parseInt(entry['Port/NickName'].split('/')[2]) > 20
      )
      for (let item of agg1Spines) {
        const device = {
          macAddress: item['MAC_Address'],
          slot: item['Port/NickName'].replace('XGE', '').split('/')[0],
          port: item['Port/NickName'].replace('XGE', '').split('/')[2],
          vlan: item['VLAN_ID'],
          role: 'Spine'
        }
        const ip = coreSiteRouter.arp.filter(entry => entry['MAC address'] == item['MAC_Address'])
        if (ip.length > 0) {
          device.ipAddress = ip[0]['IP address']
        }
        if (macAddSet.has(device.macAddress)) continue
        macAddSet.add(device.macAddress)
        if (device.slot == 1 || device.slot == 3) {
          rack13.devices.push(device)
        } else {
          rack24.devices.push(device)
        }
      }

      // get agg2 spines
      const agg2Spines = agg2['mac_adds'].filter(
        entry => /504|505|506|507/.test(entry['VLAN_ID']) &&
        entry['Port/NickName'].indexOf('XGE') != -1 &&
        coreSiteRouter.arp.filter(item => item['MAC address'] == entry['MAC_Address']).length > 0 &&
        !macAddSet.has(entry['MAC_Address']) &&
        parseInt(entry['Port/NickName'].split('/')[2]) > 20
      )
      for (let item of agg2Spines) {
        const device = {
          macAddress: item['MAC_Address'],
          slot: item['Port/NickName'].replace('XGE', '').split('/')[0],
          port: item['Port/NickName'].replace('XGE', '').split('/')[2],
          vlan: item['VLAN_ID'],
          role: 'Spine'
        }
        const ip = coreSiteRouter.arp.filter(entry => entry['MAC address'] == item['MAC_Address'])
        if (ip.length > 0) {
          device.ipAddress = ip[0]['IP address']
        }
        if (macAddSet.has(device.macAddress)) continue
        macAddSet.add(device.macAddress)
        if (device.slot == 1 || device.slot == 3) {
          rack57.devices.push(device)
        } else {
          rack68.devices.push(device)
        }
      }

      // name the racks
      for (let device of rack13.devices) {
        if (rackname.test(device.description)) {
          rack13.name = rackname.exec(device.description)[1]
          break
        }
      }
      for (let device of rack24.devices) {
        if (rackname.test(device.description)) {
          rack24.name = rackname.exec(device.description)[1]
          break
        }
      }
      for (let device of rack57.devices) {
        if (rackname.test(device.description)) {
          rack57.name = rackname.exec(device.description)[1]
          break
        }
      }
      for (let device of rack68.devices) {
        if (rackname.test(device.description)) {
          rack68.name = rackname.exec(device.description)[1]
          break
        }
      }

      // fill shared regions and push to output
      shared.racks.push(rack13)
      shared.racks.push(rack24)
      shared.racks.push(rack57)
      shared.racks.push(rack68)
      output.regions.push(shared)


      // Parse the regular tors
      for (let tor of torList) {
        // set up data structures
        const region = {region: tor.region, racks: []}
        const rack1 = {location: output.clli, devices: []}
        const rack2 = {location: output.clli, devices: []}
        const rack3 = {location: output.clli, devices: []}
        const rack4 = {location: output.clli, devices: []}

        // split the tor irf into the racks
        for (let item of tor.tor.irf) {
          const device = {
            macAddress: item['mac_address'].toLowerCase(),
            ipAddress: tor.tor.ip,
            slot: item.slot,
            description: tor.tor.lldp.local.global['sys_name'],
            role: 'TOR'
          }
          if (macAddSet.has(device.macAddress)) continue
          macAddSet.add(device.macAddress)
          if (device.slot == 1) {
            rack1.devices.push(device)
          } else if (device.slot == 2) {
            rack2.devices.push(device)
          } else if (device.slot == 3) {
            rack3.devices.push(device)
          } else if (device.slot == 4) {
            rack4.devices.push(device)
          }
        }

        // Get devices to put in racks
        const linkList = tor.tor.lldp.local.local.filter(
          item => item.name.indexOf('Gigabit') == -1 &&
          item.type == 'GigabitEthernet'
        )
        for (let link of linkList) {
          const device = {
            description: link.name,
            slot: link.interface.split('/')[0],
            port: link.interface.split('/')[2]
          }
          const lookup = 'GE' + link.interface
          const macList = tor.tor['mac_adds'].filter(mac => mac['Port/NickName'] == lookup)
          const ipList = macList.map(mac => coreSiteRouter.arp.filter(
            entry => entry['MAC address'] == mac['MAC_Address'])
          )
          if (macList.length == 0) {
            console.log(`\nDid not find a mac for ${region.region} ${lookup} ${device.description}`)
            continue
          } else if (macList.length == 1) {
            device.macAddress = macList[0]['MAC_Address']
            if (ipList[0].length == 0) {
              console.log(`\nDid not find an ip for ${macList[0]['MAC_Address']} ${region.region} ${lookup} ${device.description}`)
              continue
            }
            if (ipList[0].length == 1) {
              device.ipAddress = ipList[0][0]['IP address']
            } else {
              console.log(`\nSingle mac, multiple ip: ${region.region} ${lookup} ${device.macAddress}`)
              continue
            }
          } else {
            const checkIp = ipList.filter(item => item.length > 0)
            if (checkIp.length == 0) {
              console.log(`\nFound no ip for ${region.region} ${lookup} ${device.description}`)
            } else if (checkIp.length == 1 && checkIp[0].length == 1) {
              device.macAddress = checkIp[0][0]['MAC address']
              device.ipAddress = checkIp[0][0]['IP address']
            } else {
              for (let arr of checkIp) {
                if (arr.length > 1) console.log(`\n\n MULTPLE IPS FOUND ${region.region} ${lookup} ${device.description}`)
              }
              const highestOctet = checkIp.sort((a, b) => parseInt(b[0]['IP address'].split('.')[3]) - parseInt(a[0]['IP address'].split('.')[3]))
              device.macAddress = highestOctet[0][0]['MAC address']
              device.ipAddress = highestOctet[0][0]['IP address']
            }
          }
          if (macAddSet.has(device.macAddress)) continue
          macAddSet.add(device.macAddress)
          if (device.slot == 1) {
            rack1.devices.push(device)
          } else if (device.slot == 2) {
            rack2.devices.push(device)
          } else if (device.slot == 3) {
            rack3.devices.push(device)
          } else if (device.slot == 4) {
            rack4.devices.push(device)
          }
        }

        // gather Storage
        const storageDevices = tor.tor.arp.filter(
          entry => /^GE[1-4]\/0\/[3-4]$/.test(entry['Interface']))
        for (let item of storageDevices) {
          const device = {
            macAddress: item['MAC address'],
            ipAddress: item['IP address'],
            slot: item['Interface'].replace('GE', '').split('/')[0],
            port: item['Interface'].replace('GE', '').split('/')[2],
            role: 'Storage'
          }
          if (macAddSet.has(device.macAddress)) continue
          macAddSet.add(device.macAddress)
          if (device.slot == 1) {
            rack1.devices.push(device)
          } else if (device.slot == 2) {
            rack2.devices.push(device)
          } else if (device.slot == 3) {
            rack3.devices.push(device)
          } else if (device.slot == 4) {
            rack4.devices.push(device)
          }
        }

        // get the bigswitch leaves
        const bsLeaves = tor.tor.mac_adds.filter(
          mac => /^GE[1-4]\/0\/3[2-9]/.test(mac['Port/NickName']))
        for (let item of bsLeaves) {
          const device = {
            macAddress: item['MAC_Address'],
            slot: item['Port/NickName'].replace('GE', '').split('/')[0],
            port: item['Port/NickName'].replace('GE', '').split('/')[2],
            role: 'Leaf',
            vlan: item['VLAN_ID']
          }
          const ipList = coreSiteRouter.arp.filter(
            entry => entry['MAC address'] == device.macAddress
          )
          if (ipList.length == 0) {
            console.log(`\nNo IP found for leaf ${region.region} ${device.macAddress}`)
          } else if (ipList.length == 1) {
            device.ipAddress = ipList[0]['IP address']
          } else {
            console.log(`\nMultiple IP found for leaf ${region.region} ${device.macAddress}\n${ipList}`)
          }
          if (macAddSet.has(device.macAddress)) continue
          macAddSet.add(device.macAddress)
          if (device.slot == 1) {
            rack1.devices.push(device)
          } else if (device.slot == 2) {
            rack2.devices.push(device)
          } else if (device.slot == 3) {
            rack3.devices.push(device)
          } else if (device.slot == 4) {
            rack4.devices.push(device)
          }
        }

        // name the racks
        for (let device of rack1.devices) {
          if (rackname.test(device.description)) {
            rack1.name = rackname.exec(device.description)[1]
            break
          }
        }
        for (let device of rack2.devices) {
          if (rackname.test(device.description)) {
            rack2.name = rackname.exec(device.description)[1]
            break
          }
        }
        for (let device of rack3.devices) {
          if (rackname.test(device.description)) {
            rack3.name = rackname.exec(device.description)[1]
            break
          }
        }
        for (let device of rack4.devices) {
          if (rackname.test(device.description)) {
            rack4.name = rackname.exec(device.description)[1]
            break
          }
        }

        // fill region and push to output
        region.racks.push(rack1)
        region.racks.push(rack2)
        region.racks.push(rack3)
        region.racks.push(rack4)
        output.regions.push(region)
      }

      // Get info for monitoring racks
      if (monTor) {
        const region = {region: 'm1', racks:[]}
        const rack = {location: output.clli, devices:[]}

        // put the tor in
        for (let item of monTor.irf) {
          const device = {
            macAddress: item['mac_address'].toLowerCase(),
            ipAddress: monTor.ip,
            slot: item.slot,
            description: monTor.lldp.local.global['sys_name'],
            role: 'TOR'
          }
          if (macAddSet.has(device.macAddress)) continue
          macAddSet.add(device.macAddress)
          rack.devices.push(device)
        }

        // Add in monitoring edge
        if (monFE) {
          for (let item of monFE.irf) {
            const device = {
              macAddress: item['mac_address'].toLowerCase(),
              ipAddress: monTor.ip,
              slot: item.slot,
              description: monTor.lldp.local.global['sys_name'],
            }
            if (macAddSet.has(device.macAddress)) continue
            macAddSet.add(device.macAddress)
            rack.devices.push(device)
          }
        }

        // Get other devices
        const upLinks = monTor.interfaces.filter(
          inter => inter.state == 'UP' && /^GigabitEthernet[1-9]/.test(inter.interface)
        )
        for (let link of upLinks) {
          const device = {
            description: link.description,
            slot: link.interface.replace('GigabitEthernet', '').split('/')[0],
            port: link.interface.replace('GigabitEthernet', '').split('/')[2],
          }
          const macList = monTor.mac_adds.filter(
            item => item['Port/NickName'] == link.interface.replace('GigabitEthernet','GE')
          )
          if (macList.length == 0) continue
          if (macList.length == 1) {
            device.macAddress = macList[0]['MAC_Address']
            device.vlan = macList[0]['VLAN_ID']
          } else if (macList.length > 1) {
            // if one of the mac addresses begins with 847b, use it - that's hypermon
            const hypermonMac = macList.filter(mac => mac['MAC_Address'].indexOf('847b') == 0)
            if (hypermonMac.length == 1) {
              device.macAddress = hypermonMac[0]['MAC_Address']
              device.vlan = hypermonMac[0]['VLAN_ID']
            } else {
              console.log(`\nCould not find hypermon mac: ${device.description} ${link.interface}`)
            }
          }
          if (!device.macAddress) continue
          const ipList = coreSiteRouter.arp.filter(entry => entry['MAC address'] == device.macAddress)
          if (ipList.length == 0) {
            console.log(`\nCould not find ip address for m1 ${device.macAddress} ${device.description}`)
          } else if (ipList.length == 1) {
            device.ipAddress = ipList[0]['IP address']
          } else {
            console.log(`\nMultiple IPs for m1 ${device.macAddress} ${device.description}`)
          }
          if (macAddSet.has(device.macAddress)) continue
          macAddSet.add(device.macAddress)
          rack.devices.push(device)
        }

        // name the rack
        for (let device of rack.devices) {
          if (rackname.test(device.description)) {
            rack.name = rackname.exec(device.description)[1]
            break
          }
        }

        region.racks.push(rack)
        output.regions.push(region)
      }

      // Add security and EMC stacks
      if (secTor) {
        const region = {region: 'utility', racks:[]}
        const rack1 = {location: output.clli, name: 'storage', devices: []}
        const rack2 = {location: output.clli, name: 'security', devices: []}
        const rack3 = {location: output.clli, name: 'security', devices: []}
        const rack4 = {location: output.clli, name: 'storage', devices: []}

        // put the tors in
        for (let item of secTor.irf) {
          const device = {
            macAddress: item['mac_address'].toLowerCase(),
            ipAddress: secTor.ip,
            slot: item.slot,
            description: secTor.lldp.local.global['sys_name'],
            role: 'TOR'
          }
          if (macAddSet.has(device.macAddress)) continue
          macAddSet.add(device.macAddress)
          if (device.slot == 1) {
            rack1.devices.push(device)
          } else if (device.slot == 2) {
            rack2.devices.push(device)
          } else if (device.slot == 3) {
            rack3.devices.push(device)
          } else if (device.slot == 4) {
            rack4.devices.push(device)
          }
        }

        // find other devices
        const devices = secTor['mac_adds'].filter(
          mac => /^GE[1-9]/.test(mac['Port/NickName']))
        for (let item of devices) {
          const description = secTor.interfaces.filter(
            inter => inter.interface == item['Port/NickName'].replace('GE', 'GigabitEthernet')
          )[0].description
          const device = {
            macAddress: item['MAC_Address'],
            description: description,
            slot: item['Port/NickName'].replace('GE', '').split('/')[0],
            port: item['Port/NickName'].replace('GE', '').split('/')[2],
            vlan: item['VLAN_ID']
          }
          const ipList = coreSiteRouter.arp.filter(
            entry => entry['MAC address'] == item['MAC_Address']
          )
          if (ipList.length == 0) {
            console.log(`\nNo ip found for utility ${item['MAC_Address']} ${item['Port/NickName']}`)
          } else if (ipList.length == 1) {
            device.ipAddress = ipList[0]['IP address']
          } else {
            device.ipAddress = ipList.sort(
              (a,b) => parseInt(a['IP address'].split('.')[3]) - parseInt(b['IP address'].split('.')[3])
            )[0]['IP address']
          }
          if (macAddSet.has(device.macAddress)) continue
          macAddSet.add(device.macAddress)
          if (device.slot == 1) {
            rack1.devices.push(device)
          } else if (device.slot == 2) {
            rack2.devices.push(device)
          } else if (device.slot == 3) {
            rack3.devices.push(device)
          } else if (device.slot == 4) {
            rack4.devices.push(device)
          }
        }

        // Get bigswitch controllers
        const bsCtls = coreSiteRouter.arp.filter(
          entry => /19|21|23/.test(entry['VLAN']) && entry['Interface'].indexOf('BAGG') == -1)
        const bsCtlSet = new Set(bsCtls.map(item => item['MAC address']))
        bsCtlSet.forEach(ctl => {
          const device = {
            macAddress: ctl,
            role: 'Bigswitch'
          }
          let lookup
          const ipList = bsCtls.filter(entry => entry['MAC address'] == ctl)
          if (ipList.length == 1) {
            device.ipAddress = ipList[0]['IP address']
            device.vlan = ipList[0]['VLAN']
            lookup = ipList[0]['Interface'].replace('XGE', 'GigabitEthernet')
          } else if (ipList.length > 1) {
            const sortedIp = ipList.sort((a, b) => parseInt(b['IP address'].split('.')[3]) - parseInt(a['IP address'].split('.')[3]))
            device.ipAddress = sortedIp[0]['IP address']
            device.vlan = sortedIp[0]['VLAN']
            lookup = sortedIp[0]['Interface'].replace('XGE', 'GigabitEthernet')
          }
          const intList = coreSiteRouter.interfaces.filter(entry => entry.interface == lookup)
          if (intList.length == 1) {
            device.description = intList[0].description
          }
          if (!macAddSet.has(device.macAddress)) {
            macAddSet.add(device.macAddress)
            if (/1|2/.test(lookup.replace('GigabitEthernet', '').split('/')[0])) {
              rack1.devices.push(device)
            } else {
              rack4.devices.push(device)
            }
          }
        })

        // name the racks
        for (let device of rack1.devices) {
          if (rackname.test(device.description)) {
            rack1.name = rackname.exec(device.description)[1]
            break
          }
        }
        for (let device of rack2.devices) {
          if (rackname.test(device.description)) {
            rack2.name = rackname.exec(device.description)[1]
            break
          }
        }
        for (let device of rack3.devices) {
          if (rackname.test(device.description)) {
            rack3.name = rackname.exec(device.description)[1]
            break
          }
        }
        for (let device of rack4.devices) {
          if (rackname.test(device.description)) {
            rack4.name = rackname.exec(device.description)[1]
            break
          }
        }

        // fill region and push to output
        region.racks.push(rack1)
        region.racks.push(rack2)
        region.racks.push(rack3)
        region.racks.push(rack4)
        output.regions.push(region)

      }

    }
  }


  // Save output as site map
  Site.findOneAndUpdate({
    name: output.name
  }, output, { upsert: true }, (error, response) => {
    if (error) {
      console.log(error)
    }
  })

  // Put discovered HPs into the Device collection
  for (let info of data) {
    for (let hp of info.irf) {
      hp.hostname = info.lldp.local.global.sys_name
      hp.softwareVersion = info.lldp.local.global.software
      const device = {
        site: output.clli,
        ipAddress: info.ip,
        macAddress: hp['mac_address'].toLowerCase() || hp['flexfabric']['mac_address'].toLowerCase(),
        collector: 'discovery',
        inventoryInfo: hp
      }
      Device.findOneAndUpdate({
        macAddress: device.macAddress
      }, device, { upsert: true }, (error, response) => {
        if (error) {
          console.log(error)
        }
      })
    }
  }
  console.log('\n')
  console.log(output)
  console.log('\n')
}

export default discoveryParser
