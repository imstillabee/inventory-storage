export const find_lite_site_router = (hp_list) => {
  let siteRouter = hp_list.filter(
    device => device.lldp.local.local.filter(
      inter => {
        return (
          inter.interface == '1/0/50' && (
            inter.name.indexOf('CL_01') != -1 ||
            inter.name.indexOf('CMN|PIP') != -1
          )
        ) || (
          inter.interface == '2/0/50' && (
            inter.name.indexOf('CL_02') != -1 ||
            inter.name.indexOf('CMN|PIP') != -1
          )
        )
      }
    ).length > 0 || device.interfaces.filter(
      inter => {
        return (
          /^Bridge-Aggregation1$/.test(inter.interface) &&
          /CE|VER/.test(inter.description)
        )
      }
    ).length > 0
  )
  if (!siteRouter.length > 0 || siteRouter.length > 1) {
    siteRouter = hp_list.filter(
      device => /\.33$/.test(device.ip)
    )
  }
  return siteRouter.length == 1
    ? siteRouter[0]
    : false
}

export const find_lite_edge = (hp_list) => {
  let edgeRouter = hp_list.filter(
    device => device.lldp.local.local.filter(
      inter => {
        return (
          (inter.interface == '1003' || inter.interface == "1001") &&
          inter.name.indexOf('AS70') != -1
        ) || (
          (inter.interface == '1004' || inter.interface == '1002') &&
          inter.name.indexOf('AS70') != -1
        )
      }
    ).length > 0 && device.interfaces.filter(
      inter => {
        return (
          /Bridge-Aggregation1$/.test(inter.interface) &&
          /CS|VSR|CT/.test(inter.description)
        )
      }
    ).length > 0 || device.interfaces.filter(
      inter => {
        return (
          /^Bridge-Aggregation100$/.test(inter.interface) &&
          /Data_Plane/.test(inter.description)
        )
      }
    ).length > 0
  )
  if (!edgeRouter.length > 0 || edgeRouter.length > 1) {
    edgeRouter = hp_list.filter(
      device => /\.51$/.test(device.ip)
    )
  }
  return edgeRouter.length == 1
    ? edgeRouter[0]
    : false
}

export const find_core_site_router = (hp_list) => {
  const siteRouter = hp_list.filter(
    device => device.interfaces.filter(
      inter => {
        return (
          /Bridge-Aggregation100(1|3)/.test(inter.interface) &&
          inter.description.indexOf('CMN') != -1
        ) || (
          /Bridge-Aggregation100(2|4)/.test(inter.interface) &&
          inter.description.indexOf('CMN') != -1
        )
      }
    ).length > 0
  )
  return siteRouter.length == 1
    ? siteRouter[0]
    : false
}

export const find_core_edge = (hp_list) => {
  let edgeRouter = hp_list.filter(
    device => device.lldp.local.local.filter(
      inter => {
        return (
          inter.interface == "1001" &&
          inter.name.indexOf('AS70') != -1
        ) || (
          inter.interface == '1002' &&
          inter.name.indexOf('AS70') != -1
        ) || (
          /100[3-4]/.test(inter.interface) &&
          /EDN|WSN|RAN/.test(inter.name)
        )
      }
    ).length > 2
  )
  if (edgeRouter.length === 0 || edgeRouter.length > 1) {
    edgeRouter = hp_list.filter(
      device => /\.2$/.test(device.ip)
    )
  }
  return edgeRouter.length == 1
    ? edgeRouter[0]
    : false
}

export const find_agg1 = hpList => {
  let agg1 = hpList.filter(device => device.interfaces.filter(inter => {
    return (
        inter.interface == 'Bridge-Aggregation12' &&
        (/(CS|VSR)$/.test(inter.description) || /TO_SITE/.test(inter.description))
      )
    }).length > 0
  )
  if (agg1.length != 1) {
    agg1 = hpList.filter(hp => /\.3$/.test(hp.ip))
  }
  return agg1.length == 1
    ? agg1[0]
    : false
}

export const find_agg2 = hp_list => {
  const agg1 = hp_list.filter(device => device.interfaces.filter(inter => {
    return (
        inter.interface == 'Bridge-Aggregation10' &&
        (/(CS|VSR)$/.test(inter.description) || /TO_SITE/.test(inter.description))
      )
    }).length > 0
  )
  return agg1.length == 1
    ? agg1[0]
    : false
}

export const find_tor = (agg, data, inter, fallback=false) => {
  if (!agg) return false
  let sysname
  const search = agg.lldp.neighbors.filter(nbor => nbor.interface == `1/0/${inter}`)
  if (search.length > 0) sysname = search[0].sysname
  let tor = data.filter(device => device.lldp.local.global['sys_name'] == sysname)
  if (tor.length != 1 && fallback) {
    const regEx = new RegExp(`\.${fallback}$`)
    tor = data.filter(device => regEx.test(device.ip))
  }
  return tor.length == 1
    ? tor[0]
    : false
}
