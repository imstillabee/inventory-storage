export const decideMacIp = (interf, macList) => {
  const output = {}
  // if any duplicate macs, just use that mac and the lowest ip
  let dupMac = ''
  const macSet = new Set()
  for (let mac of macList) {
    if (macSet.has(mac['MAC address'])) {
      dupMac = mac['MAC address']
      break
    }
    macSet.add(mac['MAC address'])
  }
  if (dupMac != '') {
    output.macAddress = macList[0]['MAC address']
    output.ipAddress = macList.reduce((min, ip) => {
      return parseInt(ip['IP address'].split('.')[3]) < parseInt(min['IP address'].split('.')[3])
        ? ip
        : min
    })['IP address']
  } else {
  // otherwise, just use the one with the largest final octet ip
    const macObject = macList.reduce((max, macObj) => {
      return parseInt(macObj['IP address'].split('.')[3]) > parseInt(max['IP address'].split('.')[3])
        ? macObj
        : max
    })
    output.macAddress = macObject['MAC address']
    output.ipAddress = macObject['IP address']
  }
  return output
}
