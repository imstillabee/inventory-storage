const findNodeId = (device) => {
  if (device.collector === 'idrac') { // idracs
    if (device['inventoryInfo']['iDRAC']['Embedded']['1-1#IDRACinfo']['DNSRacName']) {
      return device['inventoryInfo']['iDRAC']['Embedded']['1-1#IDRACinfo']['DNSRacName']
    } else {
      return null
    }
  } else if (device.collector === 'cisco') { //cisco
    return device.inventoryInfo['hostname']
  } else if (device.inventoryInfo['fabric_info']) { // bigswitch
    return device['inventoryInfo']['fabric_info']['name']
  } else if (device.collector === 'discovery'){ // hps
    return device['inventoryInfo']['hostname']
  } else if (device.collector === 'storage') { // storage arrays
    return device['inventoryInfo']['storage_array']['firmware_inventory']['storage_array']['storage_array_name']
  } else if (device.collector === 'bigswitch' && device['inventoryInfo']['service-tag']) { // bs controller
    return device['inventoryInfo']['hostname']
  } else if (device.collector === 'netapp') { // netapps
    let output
    try {
      output = [
        device['inventoryInfo']['controller1']['node'],
        device['inventoryInfo']['controller2']['node']
      ]
    } catch (err) {
      output = [null,null]
    }
    return output
  } else if (device.collector === 'gigamon') {
    return device['inventoryInfo']['Chassis']['hostname']
  } else {
    return null
  }
}

export default findNodeId
