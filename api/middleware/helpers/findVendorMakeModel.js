const findVendorInfo = (device) => {
  const info = {
    vendor: null,
    make: null,
    model: null
  }
  if (device.collector === 'idrac') { // idracs
    info.vendor = 'Dell'
    info.make = 'PowerEdge'
    info.model = device.inventoryInfo['System']['Embedded']['1']['Model'].substr(-4)
  } else if (device.collector === 'cisco') { // cisco
    info.vendor = 'Cisco'
    info.make = 'Router'
    info.model = device.inventoryInfo['powerSupply']['partNumber']
  } else if (device.inventoryInfo['fabric_info']) { // bigswitch
    info.vendor = 'Dell'
    info.make = 'Dell Networking'
    info.model = device.inventoryInfo['device_info']['product-name']
  } else if (device.collector === 'discovery'){ // hps
    info.vendor = 'HPE'
    info.make = 'Switch'
    info.model = device.inventoryInfo['device_name']
  } else if (device.collector === 'storage') { // storage arrays
    info.vendor = 'Dell'
    info.make = 'Storage'
    info.model = device.inventoryInfo['storage_array']['features_summary']['additional_feature_information']['feature_pack'].substr(6)
  } else if (device.collector === 'bigswitch' && device['inventoryInfo']['service-tag']) { // bs controller
    info.vendor = 'Dell'
    info.make = 'PowerEdge'
    info.model = `R${device.inventoryInfo['platform'].substr(4)}`
  } else if (device.collector === 'netapp') { // netapps
    let output
    try {
      output = [
        {
          vendor: 'NetApp',
          make: 'Storage/Filer',
          model: device.inventoryInfo['controller1']['model']
        },
        {
          vendor: 'NetApp',
          make: 'Storage/Filer',
          model: device.inventoryInfo['controller2']['model']
        }
      ]
    } catch(err) {
      output = [
        {
          vendor: 'NetApp',
          make: 'Storage/Filer',
          model: null
        },
        {
          vendor: 'NetApp',
          make: 'Storage/Filer',
          model: null
        }
      ]
    }
    return output
  } else if (device.collector === 'gigamon') {
    info.vendor = device.inventoryInfo['Chassis']['vendor']
    info.make = device.inventoryInfo['Chassis']['vendor']
    info.model = device.inventoryInfo['Product model']
  }
  return info
}

export default findVendorInfo
