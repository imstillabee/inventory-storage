const findSvcTag = (device) => {
  if (device.collector === 'idrac') { // idracs
    return device['inventoryInfo']['System']['Embedded']['1']['ServiceTag']
  } else if (device.collector === 'cisco') { // cisco
    return device.inventoryInfo['serialNumber']
  } else if (device.inventoryInfo['fabric_info']) { // bigswitch
    return device['inventoryInfo']['device_info']['service-tag']
  } else if (device.collector === 'discovery'){ // hps
    if (device.inventoryInfo.flexfabric) {
      return device.inventoryInfo.flexfabric['device_serial_number']
    }
    return device['inventoryInfo']['device_serial_number']
  } else if (device.collector === 'storage') { // storage arrays
    return device['inventoryInfo']['storage_array']['storage_array_information_and_settings']['chassis_serial_number'].substr(4)
  } else if (device.collector === 'bigswitch' && device['inventoryInfo']['service-tag']) { // bs controller
    return device['inventoryInfo']['service-tag']
  } else if (device.collector === 'netapp') { // netapps
    let output
    try {
      output = [
        device['inventoryInfo']['controller1']['serial-number'],
        device['inventoryInfo']['controller2']['serial-number']
      ]
    } catch(err) {
      output = [null,null]
    }
    return output
  } else if (device.collector === 'gigamon') {
    return device['inventoryInfo']['Chassis']['serial-num']
  }
}

export default findSvcTag
