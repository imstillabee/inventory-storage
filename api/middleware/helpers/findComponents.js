import { hpComps, storageComps, ciscoComps, gigamonComps, netappComps, idracComps } from './componentHelpers'

const findComponents = (device, allDellFirmware=false) => {
  if (device.collector === 'idrac') { // idracs
    return idracComps(device)
  } else if (device.collector === 'cisco') { // cisco
    return ciscoComps(device)
  } else if (device.inventoryInfo['fabric_info']) { // bigswitch
    return []
  } else if (device.collector === 'discovery'){ // hps
    return hpComps(device)
  } else if (device.collector === 'storage') { // storage arrays
    return storageComps(device)
  } else if (device.collector === 'bigswitch' && device['inventoryInfo']['service-tag']) { // bs controller
    return []
  } else if (device.collector === 'netapp') { // netapps
    return netappComps(device)
  } else if (device.collector == 'gigamon') {
    return gigamonComps(device)
  } else {
    return []
  }
}

export default findComponents
