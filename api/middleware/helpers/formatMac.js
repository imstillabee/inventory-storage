const formatMac = (macAdd) => {
  const mac = macAdd.replace(new RegExp('-', 'g'), '').toUpperCase()
  const macAddress = `${mac.substr(0,2)}:${mac.substr(2,2)}:${mac.substr(4,2)}:${mac.substr(6,2)}:${mac.substr(8,2)}:${mac.substr(10,2)}`
  return macAddress
}

export default formatMac
