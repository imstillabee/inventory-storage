const findFirmware = (device, allDellFirmware=false) => {
  if (device.collector === 'idrac') { // idracs
    if (device['inventoryInfo']['iDRAC']['Embedded']['1-1#IDRACinfo']['FirmwareVersion']) {
      const idrac = device['inventoryInfo']['iDRAC']['Embedded']['1-1#IDRACinfo']['FirmwareVersion']
      const bios = device.inventoryInfo['System']['Embedded']['1']['BIOSVersionString']
      const lifecycle = device.inventoryInfo['System']['Embedded']['1']['LifecycleControllerVersion']
      if (allDellFirmware) return `idrac - ${idrac}; bios - ${bios}; lifecycle - ${lifecycle}`
      return idrac
    } else {
      return null
    }
  } else if (device.collector === 'cisco') { // cisco
    return device.inventoryInfo['softwareVersion']
  } else if (device.inventoryInfo['fabric_info']) { // bigswitch
    if (!allDellFirmware) return device['inventoryInfo']['device_info']['onie-version']
    const onie = device['inventoryInfo']['device_info']['onie-version']
    const software = device['inventoryInfo']['fabric_info']['software-description']
    return `onie - ${onie}; software - ${software}`
  } else if (device.collector === 'discovery'){ // hps
    return device['inventoryInfo']['softwareVersion']
  } else if (device.collector === 'storage') { // storage arrays
    return device['inventoryInfo']['storage_array']['firmware_inventory']['storage_array']['current_package_version']
  } else if (device.collector === 'bigswitch' && device['inventoryInfo']['service-tag']) { // bs controller
    if (!allDellFirmware) return device['inventoryInfo']['release']
    const software = device['inventoryInfo']['release']
    return `software - ${software}`
  } else if (device.collector === 'netapp') { // netapps
    let output
    try {
      output = [
        device['inventoryInfo']['controller1']['components']['NetApp Release']['NetApp Release'],
        device['inventoryInfo']['controller2']['components']['NetApp Release']['NetApp Release']
      ]
    } catch(err) {
      output = [null,null]
    }
    return output
  } else if (device.collector == 'gigamon') {
    return device['inventoryInfo']['Chassis']['sw-rev']
  } else {
    return null
  }
}

export default findFirmware
