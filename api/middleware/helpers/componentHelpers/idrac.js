const recurse = (obj) => {
  const keys = Object.keys(obj)
  if (keys.length > 1) {
    return obj
  } else {
    return recurse(obj[keys[0]])
  }
}

const idracComps = (device) => {
  const components = []
  if (device.inventoryInfo) {
    if (device.inventoryInfo.PSU) {
      for (let key in device.inventoryInfo.PSU.Slot) {
        const psu = {
          vendor: device.inventoryInfo.PSU.Slot[key].Manufacturer,
          make: device.inventoryInfo.PSU.Slot[key].Model,
          model: device.inventoryInfo.PSU.Slot[key].PartNumber,
          firmware: device.inventoryInfo.PSU.Slot[key].FirmwareVersion,
          serial: device.inventoryInfo.PSU.Slot[key].SerialNumber
        }
        components.push(psu)
      }
    }
    if (device.inventoryInfo.Bay) {
      for (let key in device.inventoryInfo.Bay) {
        const info = recurse(device.inventoryInfo.Bay[key])
        const component = {}
        if (info.SerialNumber) {
          component.serial = info.SerialNumber
          if (info.Manufacturer) component.vendor = info.Manufacturer
          if (info.Model) component.model = info.Model
          if (info.MediaType) component.make = info.MediaType
          components.push(component)
        }
      }
    }
    if (device.inventoryInfo.DIMM) {
      for (let key in device.inventoryInfo.DIMM.Socket) {
        const dimm = {
          vendor: device.inventoryInfo.DIMM.Socket[key].Manufacturer,
          model: device.inventoryInfo.DIMM.Socket[key].Model,
          serial: device.inventoryInfo.DIMM.Socket[key].SerialNumber
        }
        components.push(dimm)
      }
    }

  }
  return components
}

export default idracComps
