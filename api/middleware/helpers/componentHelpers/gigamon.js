const gigamonComps = (device) => {
  const components = []
  if (device.inventoryInfo) {
    for (let card in device.inventoryInfo.Card) {
      const newCard = {
        serial: device.inventoryInfo.Card[card]['Serial Num'],
        model: device.inventoryInfo.Card[card][' HW Type']
      }
      components.push(newCard)
    }
  }
  return components
}

export default gigamonComps
