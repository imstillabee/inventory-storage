const netappComps = (device) => {
  const components = []
  if (device.inventoryInfo) {
    if (device.inventoryInfo.controller1) {
      if (device.inventoryInfo.controller1.components) {
        for (let serial of device.inventoryInfo.controller1.components.disk.serialnum) {
          components.push({serial: serial})
        }
      }
    }
  }
  return [components, components]
}

export default netappComps
