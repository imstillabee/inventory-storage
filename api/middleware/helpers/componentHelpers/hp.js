const hpComps = (device) => {
  const components = []
  if (device.inventoryInfo.power) {
    device.inventoryInfo.power.map(psu => {
      components.push({
        serial: psu['manu_serial_number']
      })
    })
  }
  if (device.inventoryInfo.fan) {
    device.inventoryInfo.fan.map(fan => {
      components.push({
        serial: fan['device_serial_number'],
        vendor: fan['vendor_name'],
        make: fan['vendor_name'],
        model: fan['device_name']
      })
    })
  }
  return components
}

export default hpComps
