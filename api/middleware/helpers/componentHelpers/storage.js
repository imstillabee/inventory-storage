const storageComps = (device) => {
  const components = []
  if (device.inventoryInfo) {
    if (device.inventoryInfo.enclosures) {
      for (let powerSupply of device.inventoryInfo.enclosures.power) {
        const newPS = {
          vendor: powerSupply.vendor.substr(3),
          make: 'Power Supply',
          model: powerSupply['part_number'],
          serial: powerSupply['serial_number'].substr(3),
          firmware: powerSupply['firmware_version'],
        }
        components.push(newPS)
      }
      for (let fan of device.inventoryInfo.enclosures.fans) {
        const newFan = {
          vendor: fan.vendor.substr(3),
          make: "Fan",
          model: fan['part_number'].substr(3),
          serial: fan['serial_number'].substr(3)
        }
        components.push(newFan)
      }
      for (let drawer of device.inventoryInfo.enclosures.drawers) {
        const newDrawer = {
          vendor: drawer.manufacturer.substr(3),
          make: 'Drawer',
          model: drawer['part_number'].substr(3),
          serial: drawer['serial_number'].substr(3)
        }
        components.push(newDrawer)
      }
      for (let battery of device.inventoryInfo.enclosures.batteries) {
        const newBattery = {
          vendor: battery.vendor.substr(3),
          make: "Battery",
          model: battery['part_number'].substr(3),
          serial: battery['serial_number'].substr(3)
        }
        components.push(newBattery)
      }
    }
    if (device.inventoryInfo['physical_disks']) {
      for (let disk of device.inventoryInfo['physical_disks']) {
        const newDisk = {
          vendor: disk.make,
          make: disk.make,
          model: disk.model,
          firmware: disk.firmware,
          serial: disk.serial
        }
        components.push(newDisk)
      }
    }
  }
  return components
}

export default storageComps
