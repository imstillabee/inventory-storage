const ciscoComps = (device) => {
  const components = []
  if (device.inventoryInfo) {
    for (let nic of device.inventoryInfo.Nic) {
      const newNic = {
        make: "NIC",
        model: nic.modelNumber,
        serial: nic.serialNumber
      }
      components.push(newNic)
    }
    for (let powerSupply of device.inventoryInfo.powerSupply) {
      const newPS = {
        make: "Power Supply",
        model: powerSupply.partNumber,
        serial: powerSupply.serialNumber || "not provided"
      }
      components.push(newPS)
    }
  }
  return components
}

export default ciscoComps
