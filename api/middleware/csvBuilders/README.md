# CSV Builder Middleware
Logic for constructing various CSVs for download at the /csv endpoint

## Files
 - index.js is the central file for exporting functions from the csvBuilder folder.
 - loops.js contains functions that loop through the data provided to the csvbuilder and calls the function passed to it by the controller
 - The rest of the files are the functions that the controller passes to the loops.js function, along with the headers for the CSV the function builds. These function compose an array that is converted to a CSV file in the controller for their respective CSVs.

## Process
Based on the endpoint, the controller calls the appropriate loop with data pulled from the database. For each device in that data, the loop calls the function from one of the other files that constructs the array that will translate to one line of the final CSV. These functions rely on limited special logic, preferring to use functions from the middleware/helpers folder where possible.
