import { findSvcTag, formatMac, findNodeId,
  findFirmware, findVendorInfo } from '../helpers'

export const buildDells = (name, device, site) => {
  const macAddress = formatMac(device.macAddress)
  const line = [[site, name, '', macAddress, '', '', '']]
  if (device.description) line[0][5] = device.description
  if (device.ipAddress) line[0][2] = device.ipAddress
  if (device.info) {
    if (device.info.ipAddress) line[0][2] = device.info.ipAddress
    if (device.info.collector != 'idrac' && device.info.collector != 'bigswitch') {
      return false
    }
    const svcTag = findSvcTag(device.info)
    const description = findNodeId(device.info)
    const firmware = findFirmware(device.info, true)
    const vendorInfo = findVendorInfo(device.info)
    line[0][4] = svcTag
    line[0][5] = description
    line[0][6] = firmware
    line[0][7] = vendorInfo.vendor
    line[0][8] = vendorInfo.make
    line[0][9] = vendorInfo.model
    return line
  }
  return false
}

export const dellHeaders = [['Site', 'Rack', 'IP Address', 'Mac Address', 'Service Tag', 'Description', 'Firmware Version', 'Vendor', 'Make', 'Model']]
