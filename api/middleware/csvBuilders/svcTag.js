import { findSvcTag, formatMac, findNodeId,
  findFirmware, findVendorInfo } from '../helpers'

export const buildSvcTag = (name, device, site) => {
  const macAddress = formatMac(device.macAddress)
  const line = [[site, name, '', macAddress, '', '', '', '', '', '']]
  if (device.description) line[0][5] = device.description
  if (device.ipAddress) line[0][2] = device.ipAddress
  if (device.info) {
    if (device.info.ipAddress && !(device.info.collector == 'storage')) line[0][2] = device.info.ipAddress
    const svcTag = findSvcTag(device.info)
    const description = findNodeId(device.info)
    const firmware = findFirmware(device.info, true)
    const vendorInfo = findVendorInfo(device.info)
    if (Array.isArray(svcTag)) {
      line[0][4] = svcTag[0]
      line[0][5] = description[0]
      line[0][6] = firmware[0]
      line[0][7] = vendorInfo[0].vendor
      line[0][8] = vendorInfo[0].make
      line[0][9] = vendorInfo[0].model
      line.push([
        site,
        name,
        device.info.ipAddress || device.ipAddress,
        macAddress,
        svcTag[1],
        description[1],
        firmware[1],
        vendorInfo[1].vendor,
        vendorInfo[1].make,
        vendorInfo[1].model
      ])
    } else {
      line[0][4] = svcTag
      line[0][5] = description
      line[0][6] = firmware
      line[0][7] = vendorInfo.vendor
      line[0][8] = vendorInfo.make
      line[0][9] = vendorInfo.model
    }
    return line
  }
  if (device.description){
    if (device.description.indexOf('NETAPP') != -1) return []
  }
  return [[site, name, device.ipAddress, macAddress, '', device.description, '', '', '']]
}

export const svcTagHeaders = [['Site', 'Rack', 'IP Address', 'Mac Address', 'Service Tag', 'Description', 'Firmware Version', 'Vendor', 'Make', 'Model']]
