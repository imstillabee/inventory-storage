import { findSvcTag, formatMac, findNodeId,
  findFirmware, findVendorInfo } from '../helpers'

export const buildLeafSpine = (name, device, site) => {
  const macAddress = formatMac(device.macAddress)
  const line = [['', '', '', '', '']]
  if (device.description) line[0][0] = device.description
  if (device.info) {
    if (device.info.collector != 'bigswitch') return false
    if (!device.info.inventoryInfo['device_info']) return false
    const svcTag = findSvcTag(device.info)
    const description = findNodeId(device.info)
    const vendorInfo = findVendorInfo(device.info)
    const mfgDate = device.info.inventoryInfo['device_info']['manufacture-date']
    line[0][0] = description
    line[0][1] = svcTag
    line[0][2] = mfgDate
    line[0][3] = vendorInfo.vendor
    line[0][4] = vendorInfo.model
    return line
  }
  return false
}

export const leafSpineHeaders = [['Hostname', 'Service Tag', 'Mfg Date', 'Vendor', 'Model']]
