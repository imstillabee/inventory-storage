import { findSvcTag, formatMac, findNodeId } from '../helpers'

export const buildInventory = (name, device, site) => {
  const macAddress = formatMac(device.macAddress)
  const line = [[name, '', '', macAddress]]
  if (device.description) line[0][2] = device.description
  if (device.info) {
    const svcTag = findSvcTag(device.info)
    const description = findNodeId(device.info)
    if (Array.isArray(svcTag)) {
      line[0][1] = svcTag[0]
      if (description) line[0][2] = description[0]
      line.push([
        name,
        svcTag[1],
        description[1],
        formatMac(device['info']['inventoryInfo']['controller_2']['mac_address'])
      ])
    } else {
      line[0][1] = svcTag
      if (description) line[0][2] = description
    }
    return line
  }
  if (device.description){
    if (device.description.indexOf('NETAPP') != -1) return []
  }
  return [[name, '', device.description, macAddress]]
}

export const inventoryHeaders = [['Rack', 'Service Tag', 'Description', 'Mac Address']]
