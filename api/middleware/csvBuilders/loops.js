export const parseSites = (sites, headers, buildLine) => {
  const lines = headers
  for (let site of sites) {
    for (let region of site.regions) {
      for (let rack of region.racks) {
        const name = rack.name ? rack.name : 'unknown'
        for (let device of rack.devices) {
          if (!device.macAddress) continue
          const line = buildLine(name, device, site.clli)
          if (line) {
            for (let entry of line) {
              lines.push(entry)
            }
          }
        }
      }
    }
  }
  return lines
}

export const parseRegion = (regions, headers, buildLine, site) => {
  const lines = headers
  for (let region of regions) {
    for (let rack of region.racks) {
      const name = rack.name ? rack.name : 'unknown'
      for (let device of rack.devices) {
        if (!device.macAddress) continue
        const line = buildLine(name, device, site)
        if (line) {
          for (let entry of line) {
            lines.push(entry)
          }
        }
      }
    }
  }
  return lines
}

export const parseRack = (racks, headers, buildLine, site) => {
  const lines = headers
  for (let rack of racks) {
    const name = rack.name ? rack.name : 'unknown'
    for (let device of rack.devices) {
      if (!device.macAddress) continue
      const line = buildLine(name, device, site)
      if (line) {
        for (let entry of line) {
          lines.push(entry)
        }
      }
    }
  }
  return lines
}
