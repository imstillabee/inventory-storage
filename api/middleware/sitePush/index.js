import findRegions from './findRegions'
import axios from 'axios'
import https from 'https'

let config = require('../../../config')

const invAPI = axios.create({
  baseURL: config.apiUrl,
  timeout: 60000,
  headers: {'Content-Type': 'application/json'},
  httpsAgent: new https.Agent({
    rejectUnauthorized: false
  })
})

async function sitePush(siteCode) {
  const output = {
    clli: siteCode,
    regions: []
  }
  output.regions = await findRegions(siteCode)
  const response = await invAPI.post(`/import/sites/${output.clli}`, JSON.stringify(output))
    .then(response => {
      console.log(JSON.stringify(output))
      console.log(response.status)
      console.log(response.data)
      return {'success': true}
    })
    .catch(error => {
      if (error.response) {
        console.log(JSON.stringify(output))
        console.log(error.response.status)
        console.log(error.response.data)
      } else if (error.request) {
        console.log(JSON.stringify(output))
        console.log(error.request)
        console.log('no response recieved')
      } else {
        console.log('ERROR\n', error.message)
      }
      return {'success': false}
    })
  return response
}

export default sitePush
