import Site from '../../models/Site'
import { findSvcTag, formatMac, findNodeId, findFirmware, findVendorInfo, findComponents } from '../helpers'

const findRegions = async (clli) => {
  const stacks = []
  const site = await Site.findOne({clli: clli})
                         .populate('regions.racks.devices.info', '-_id')
                         .exec((err, site) => {
    if (err) throw err
    if (!site) {
      return false
    } else if (!site.regions) {
      return false
    } else {
      site.regions.map(stack => {
        const region = {
          name: stack.region,
          racks: []
        }
        stack.racks.map(rack => {
          const cabinet = {
            units: [],
            fingerprint: 'unknown'
          }
          if (rack.name && !(clli == "ARTOTXLP")) {
            cabinet.fingerprint = rack.name.replace('X', '.').replace(/^[0]+/g,"")
          } else if (rack.name && clli == "ARTOTXLP") {
            cabinet.fingerprint = rack.name
          }
          rack.devices.map(device =>{
            let ipAddress
            if (device.info) {
              if (device.info.ipAddress) {
                ipAddress = device.info.ipAddress
              } else if (device.ipAddress) {
                ipAddress = device.ipAddress
              }
            }
            const macAdd = formatMac(device.macAddress)
            const serial = device.info ? findSvcTag(device.info) : undefined
            const nodeID = device.info ? findNodeId(device.info) : undefined
            const firmware = device.info ? findFirmware(device.info) : undefined
            const vendorInfo = device.info ? findVendorInfo(device.info) : {}
            const number = parseInt(device.port ? device.port : '1')
            const index = cabinet.units.findIndex(element => element.number === number)
            const components = device.info ? findComponents(device.info) : undefined
            if (Array.isArray(serial)){ // then the device is a netapp
              const netapp1 = {
                ipAddress: ipAddress,
                macAddress: macAdd,
                serial: String(serial[0]),
                nodeIdentifier: String(nodeID[0]),
                firmware: String(firmware[0])
              }
              const netapp2 = {
                ipAddress: ipAddress,
                macAddress: formatMac(device.info.inventoryInfo['controller2']['mac']),
                serial: String(serial[1]),
                nodeIdentifier: String(nodeID[1]),
                firmware: String(firmware[0])
              }
              if (Array.isArray(vendorInfo)) {
                netapp1.vendor = vendorInfo[0].vendor
                netapp1.make = vendorInfo[0].make
                netapp1.model = vendorInfo[0].model
                netapp2.vendor = vendorInfo[1].vendor
                netapp2.make = vendorInfo[1].make
                netapp2.model = vendorInfo[1].model
              }
              if (Array.isArray(components)){
                netapp1.components = components[0]
                netapp2.components = components[1]
              }
              if (index === -1) {
                cabinet.units.push({
                  number: number,
                  devices: [ netapp1, netapp2 ]
                })
              } else {
                cabinet.units[index].devices.push(netapp1)
                cabinet.units[index].devices.push(netapp2)
              }
            } else {
              if (index === -1) {
                cabinet.units.push({
                  number: number,
                  devices: [
                    {
                      ipAddress: ipAddress,
                      macAddress: macAdd,
                      serial: String(serial),
                      nodeIdentifier: String(nodeID),
                      firmware: String(firmware),
                      vendor: vendorInfo.vendor,
                      make: vendorInfo.make,
                      model: String(vendorInfo.model),
                      components: components
                    }
                  ]
                })
              } else {
                cabinet.units[index].devices.push({
                  ipAddress: ipAddress,
                  macAddress: macAdd,
                  serial: String(serial),
                  nodeIdentifier: String(nodeID),
                  firmware: String(firmware),
                  vendor: vendorInfo.vendor,
                  make: vendorInfo.make,
                  model: vendorInfo.model,
                  components: components
                })
              }
            }
          })
          region.racks.push(cabinet)
        })
        stacks.push(region)
      })
    }
  })
  return stacks
}

export default findRegions
