import { Router } from 'express'
import { get } from '../../controllers/pushesController'

const route = Router()

route.get('/:clli', get)

export default route
