import { Router } from 'express'
import {
  post, getSite,
  getName, getRegion,
  getRackName
} from '../../controllers/siteController'

const route = Router()

route.get('/', getSite)
route.get('/:name', getName)
route.get('/:name/:region', getRegion)
route.get('/:name/:region/:rackname', getRackName)

export default route
