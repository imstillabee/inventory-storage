import { Router } from 'express'
import devices from './devices'
import sites from './sites'
import csvs from './csvs'
import pushes from './pushes'
import discovery from './discovery'

const route = Router()

route.use('/devices', devices)
route.use('/sites', sites)
route.use('/csv', csvs)
route.use('/push', pushes)
route.use('/discovery', discovery)

export default route
