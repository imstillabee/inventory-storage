import { Router } from 'express'
import { post, get, getMac } from '../../controllers/deviceController'

const route = Router()

route.post('/', post)
route.get('/', get)
route.get('/:macAddress', getMac)

export default route
