import { Router } from 'express'
import { post, getName } from '../../controllers/discoveryController'

const route = Router()

route.post('/', post)
route.get('/:name', getName)

export default route
