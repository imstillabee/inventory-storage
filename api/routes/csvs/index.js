import { Router } from 'express'
import {
  index,
  getAll,
  getSite,
  getRegion,
  getRack
} from '../../controllers/csvController'

const route = Router()

route.get('/', index)
route.get('/:csv', getAll)
route.get('/:csv/:name', getSite)
route.get('/:csv/:name/:region', getRegion)
route.get('/:csv/:name/:region/:rack', getRack)

export default route
