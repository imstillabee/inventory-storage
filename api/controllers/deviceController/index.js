import { Device } from '../../models'

export function post(req, res) {
  const data = req.body
  Device.findOneAndUpdate({
    macAddress: data.macAddress
  }, data, { upsert: true }, (error, response) => {
    if (error) {
      res.send(error)
    } else {
      res.send(response)
    }
  })
}
export function get(req, res) {
  Device.find(req.query, '-_id', (err, devices) => {
    if (err) {
      console.log(err)
      res.send(err)
    } else {
      res.send(devices)
    }
  })
}
export function getMac(req, res) {
  Device.findOne({macAddress: req.params.macAddress}, '-_id', (err, devices) => {
    if (err) {
      console.log(err)
      res.send(err)
    } else {
      res.send(devices)
    }
  })
}
