import { Discovery } from '../../models'

export function post(req, res) {
  Discovery.findOneAndUpdate({
    site: req.body.site
  }, req.body, { upsert: true }, (error, response) => {
    if (error) {
      res.send(error)
    } else {
      res.send(response)
    }
  })
}
export function getName(req, res) {
  Discovery.findOne({$or: [{site: req.params.name}, {clli: req.params.name}]}, (err, sites) => {
      if (err) {
        console.log(err)
        res.send(err)
      } else {
        res.send(sites)
      }
    })
}
