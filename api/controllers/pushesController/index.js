import sitePush from '../../middleware/sitePush'

export const get = async (req, res) => {
  const output = await sitePush(req.params.clli)
  res.json(output)
}
