import { Site } from '../../models'
import { parseSites, parseRegion, parseRack,
  buildSvcTag, svcTagHeaders,
  buildInventory, inventoryHeaders,
  buildDells, dellHeaders,
  buildLeafSpine, leafSpineHeaders } from '../../middleware/csvBuilders'
import stringify from 'csv-stringify'

export const index = (req, res) => {
  res.json([
    {
      title: 'Service Tag Summary',
      headers: ['Site', 'Rack', 'IP Address', 'Mac Address', 'Service Tag', 'Description', 'Firmware Version', 'Vendor', 'Make', 'Model'],
      endpoints: {
        all: '/svc-tag',
        site: '/svc-tag/:clli',
        region: '/svc-tag/:clli/:region',
        rack: '/svc-tag/:clli/:region/:rack'
      }
    },
    {
      title: 'Dell Report',
      headers: ['Site', 'Rack', 'IP Address', 'Mac Address', 'Service Tag', 'Description', 'Firmware Version', 'Vendor', 'Make', 'Model'],
      endpoints: {
        all: '/dell',
        site: '/dell/:clli',
        region: '/dell/:clli/:region',
        rack: '/dell/:clli/:region/:rack'
      }
    },
    {
      title: 'Inventory',
      headers: ['Rack', 'Service Tag', 'Description', 'Mac Address'],
      endpoints: {
        all: '/inventory',
        site: '/inventory/:clli',
        region: '/inventory/:clli/:region',
        rack: '/inventory/:clli/:region/:rack'
      }
    },
    {
      title: 'Lead/Spine Audit',
      headers: ['Hostname', 'Service Tag', 'Mfg Date', 'Vendor', 'Model'],
      endpoints: {
        all: '/leaf-spine',
        site: '/leaf-spine/:clli',
        region: '/leaf-spine/:clli/:region',
        rack: '/leaf-spine/:clli/:region/:rack'
      }
    }
  ])
}

export const getAll = (req, res) => {
  Site.find({}, '-_id -regions._id -regions.racks._id')
  .populate('regions.racks.devices.info', '-_id')
  .exec((err, sites) => {
    if (err) {
      console.log(err)
      res.send(err)
    } else {
      if (sites.length === 0){
        res.status(404).send([{error: 'no site found'}])
      } else {
        let lines
        if (req.params.csv === 'svc-tag') {
          lines = parseSites(sites, svcTagHeaders.slice(), buildSvcTag)
        } else if (req.params.csv === 'inventory') {
          lines = parseSites(sites, inventoryHeaders.slice(), buildInventory)
        } else if (req.params.csv === 'dell') {
          lines = parseSites(sites, dellHeaders.slice(), buildDells)
        } else if (req.params.csv === 'leaf-spine') {
          lines = parseSites(sites, leafSpineHeaders.slice(), buildLeafSpine)
        }
        stringify(lines, (err, output) => {
          if (err) {
            console.log(err)
            res.send(err)
          } else {
            res.set({'Content-Type': 'application/force-download','Content-disposition':`attachment; filename=${req.params.csv}-summary.csv`})
            res.send(output)
          }
        })
      }
    }
  })
}

export const getSite = (req, res) => {
  Site.find({$or: [{name: req.params.name}, {clli: req.params.name}]}, '-_id -regions._id -regions.racks._id')
  .populate('regions.racks.devices.info', '-_id')
  .exec((err, sites) => {
    if (err) {
      console.log(err)
      res.send(err)
    } else {
      if (sites.length === 0){
        res.status(404).send([{error: 'no site found'}])
      } else {
        let lines
        if (req.params.csv === 'svc-tag') {
          lines = parseSites(sites, svcTagHeaders.slice(), buildSvcTag)
        } else if (req.params.csv === 'inventory') {
          lines = parseSites(sites, inventoryHeaders.slice(), buildInventory)
        } else if (req.params.csv === 'dell') {
          lines = parseSites(sites, dellHeaders.slice(), buildDells)
        } else if (req.params.csv === 'leaf-spine') {
          lines = parseSites(sites, leafSpineHeaders.slice(), buildLeafSpine)
        }
        stringify(lines, (err, output) => {
          if (err) {
            console.log(err)
            res.send(err)
          } else {
            res.set({'Content-Type': 'application/force-download','Content-disposition': `attachment; filename=${req.params.csv}-summary-${req.params.name}.csv`})
            res.send(output)
          }
        })
      }
    }
  })
}

export const getRegion = (req, res) => {
  Site.find({$or: [{name: req.params.name}, {clli: req.params.name}]}, '-_id -regions._id -regions.racks._id')
  .populate('regions.racks.devices.info', '-_id')
  .exec((err, sites) => {
    if (err) {
      console.log(err)
      res.send(err)
    } else {
      if (sites.length === 0){
        res.status(404).send([{error: 'no site found'}])
      } else if (!sites[0].regions) {
        res.status(404).send([{error: 'no region found'}])
      } else {
        const regions = sites[0].regions.filter(region => region.region === req.params.region)
        if (regions.length === 0){
          res.status(404).send([{error: 'no region found'}])
        } else {
          let lines
          if (req.params.csv === 'svc-tag') {
            lines = parseRegion(regions, svcTagHeaders.slice(), buildSvcTag, sites[0].clli)
          } else if (req.params.csv === 'inventory') {
            lines = parseRegion(regions, inventoryHeaders.slice(), buildInventory, sites[0].clli)
          } else if (req.params.csv === 'dell') {
            lines = parseRegion(regions, dellHeaders.slice(), buildDells, sites[0].clli)
          } else if (req.params.csv === 'leaf-spine') {
            lines = parseRegion(regions, leafSpineHeaders.slice(), buildLeafSpine)
          }
          stringify(lines, (err, output) => {
            if (err) {
              console.log(err)
              res.send(err)
            } else {
              res.set({'Content-Type': 'application/force-download','Content-disposition': `attachment; filename=${req.params.csv}-summary-${req.params.name}-${req.params.region}.csv`})
              res.send(output)
            }
          })
        }
      }
    }
  })
}

export const getRack = (req, res) => {
  Site.find({$or: [{name: req.params.name}, {clli: req.params.name}]}, '-_id -regions._id -regions.racks._id')
  .populate('regions.racks.devices.info', '-_id')
  .exec((err, sites) => {
    if (err) {
      console.log(err)
      res.send(err)
    } else {
      if (sites.length === 0){
        res.status(404).send([{error: 'no site found'}])
      } else if (!sites[0].regions) {
        res.status(404).send([{error: 'no region found'}])
      } else {
        const regions = sites[0].regions.filter(region => region.region === req.params.region)
        if(regions.length === 0){
          res.status(404).json([{error: 'no region found'}])
        } else {
          const racks = regions[0].racks.filter(rack => rack.name === req.params.rack)
          if (racks.length === 0) {
            res.status(404).json([{error: 'no rack found'}])
          } else {
            let lines
            if (req.params.csv === 'svc-tag') {
              lines = parseRack(racks, svcTagHeaders.slice(), buildSvcTag, sites[0].clli)
            } else if (req.params.csv === 'inventory') {
              lines = parseRack(racks, inventoryHeaders.slice(), buildInventory, sites[0].clli)
            } else if (req.params.csv === 'dell') {
              lines = parseRack(racks, dellHeaders.slice(), buildDells, sites[0].clli)
            } else if (req.params.csv === 'leaf-spine') {
              lines = parseRack(racks, leafSpineHeaders.slice(), buildLeafSpine)
            }
            stringify(lines, (err, output) => {
              if (err) {
                console.log(err)
                res.send(err)
              } else {
                res.set({'Content-Type': 'application/force-download','Content-disposition': `attachment; filename=${req.params.csv}-summary-${req.params.name}-${req.params.region}-${req.params.rack}.csv`})
                res.send(output)
              }
            })
          }
        }
      }
    }
  })
}
