import { Site } from '../../models'

export function getSite(req, res) {
  const query = {}
  if (req.query.type) query['type'] = req.query.type
    if (parseInt(req.query.depth)) {
      Site.find(query, '-_id -regions._id -regions.racks._id')
      .populate('regions.racks.devices.info', '-_id')
      .exec((err, sites) => {
        if (err) {
          console.log(err)
          res.send(err)
        } else if (sites.length === 0){
          res.status(404).send([{error: 'no site found'}])
        } else {
          res.send(sites)
        }
      })
    } else {
        Site.find(query, '-_id -regions._id -regions.racks._id')
        .exec((err, sites) => {
          if (err) {
            console.log(err)
            res.send(err)
          } else if (sites.length === 0){
            res.status(404).send([{error: 'no site found'}])
          } else {
            res.send(sites)
          }
        })
      }
}
export function getName(req, res) {
  if (parseInt(req.query.depth)) {
    Site.find({$or: [{name: req.params.name}, {clli: req.params.name}]}, '-_id -regions._id -regions.racks._id')
      .populate('regions.racks.devices.info', '-_id')
      .exec((err, sites) => {
        if (err) {
          console.log(err)
          res.send(err)
        } else if (sites.length === 0){
          res.status(404).send([{error: 'no site found'}])
        } else {
          res.send(sites)
        }
      })
  } else {
      Site.find({$or: [{name: req.params.name}, {clli: req.params.name}]}, '-_id -regions._id -regions.racks._id')
        .exec((err, sites) => {
          if (err) {
            console.log(err)
            res.send(err)
          } else if (sites.length === 0){
            res.status(404).send([{error: 'no site found'}])
          } else {
            res.send(sites)
          }
        })
    }
}
export function getRegion(req, res) {
  if (parseInt(req.query.depth)) {
    Site.findOne({$or: [{name: req.params.name}, {clli: req.params.name}]}, '-_id -regions._id -regions.racks._id')
      .populate('regions.racks.devices.info', '-_id')
      .exec((err, sites) => {
        if (err) {
          console.log(err)
          res.send(err)
        } else if (!sites){
          res.status(404).send([{error: 'no site found'}])
        } else if (!sites.regions) {
          res.status(404).send([{error: 'no region found'}])
        } else {
          const regions = sites.regions
          res.send(regions.filter(region => region.region === req.params.region))
        }
      })
  } else {
      Site.findOne({$or: [{name: req.params.name}, {clli: req.params.name}]}, '-_id -regions._id -regions.racks._id')
        .exec((err, sites) => {
          if (err) {
            console.log(err)
            res.send(err)
          } else if (!sites){
            res.status(404).send([{error: 'no site found'}])
          } else if (!sites.regions) {
            res.status(404).send([{error: 'no region found'}])
          } else {
            const regions = sites.regions
            res.send(regions.filter(region => region.region === req.params.region))
          }
      })
    }
}
export function getRackName(req, res) {
  if (parseInt(req.query.depth)) {
    Site.findOne({$or: [{name: req.params.name}, {clli: req.params.name}]}, '-_id -regions._id -regions.racks._id')
      .populate('regions.racks.devices.info', '-_id')
      .exec((err, sites) => {
        if (err) {
          console.log(err)
          res.send(err)
        } else if (!sites){
          res.status(404).json([{error: 'no site found'}])
        } else if (!sites.regions) {
          res.status(404).json([{error: 'no region found'}])
        } else {
          const regions = sites.regions.filter(region => region.region === req.params.region)
          if(regions.length === 0){
            res.status(404).json([{error: 'no region found'}])
          } else {
            res.json(regions[0].racks.filter(rack => rack.name === req.params.rackname))
          }
        }
      })
  } else {
      Site.findOne({$or: [{name: req.params.name}, {clli: req.params.name}]}, '-_id -regions._id -regions.racks._id')
        .exec((err, sites) => {
          if (err) {
            console.log(err)
            res.send(err)
          } else if (!sites){
            res.status(404).json([{error: 'no site found'}])
          } else if (!sites.regions) {
            res.status(404).json([{error: 'no region found'}])
          } else {
            const regions = sites.regions.filter(region => region.region === req.params.region)
            if(regions.length === 0){
              res.status(404).json([{error: 'no region found'}])
            } else {
              res.json(regions[0].racks.filter(rack => rack.name === req.params.rackname))
            }
          }
        })
    }
}
