#Mongo Replica Setup

## 1. IP ADDRESS EXPORT
Get the IP address of all three Openstack Instances and export thier address on all three instances:

```
export node1=<IP_ADDRESS>
export node2=<IP_ADDRESS>
export node3=<IP_ADDRESS>
```

## 2. HOME DIRECTORY
Create a home directory on all instances for all of our mongo setup to be stored within

```
mkdir -p /home/core
cd /home/core
```

## 3. ADMIN CREATION
On Node 1, start the following mongodb container. This will start the container with no authentication so that we can provision it. Be sure that all three of your instances floating IP's have a DNS address attached. For now we will be using the DNS of NODE1

```
docker run --name mongo \
-v /home/core/mongo-files/data:/data/db \
-v /home/core/mongo-files:/opt/keyfile \
--hostname="<NAME_OF_NODE1_DNS_ADDRESS>" \
-p 27017:27017 \
-d mongo:2.6.5 --smallfiles
```

Next we will create an Admin user that will connect to the mongo container we just booted. Begin by opening the interactive shell

`docker exec -it mongo /bin/bash`

Now you are inside the contianer, but not running the mongo shell. Do this by simply running:

`mongo`

Switch to the admin user

`use admin`

Then create a new site admin user. You can name the user and password whatever you desire, but the roles object should stay the same

```
db.createUser( {
     user: "inv-admin",
     pwd: "ganondorf",
     roles: [ { role: "userAdminAnyDatabase", db: "admin" } ]
   });
```

You should see a successful message like the following:

```
Successfully added user: {
"user" : "siteUserAdmin",
"roles" : [
         {
              "role" : "userAdminAnyDatabase",
              "db" : "admin"
         }
      ]
}
```

Next we will creeate a root user. Again name the user and password whatever you want, but leave the roles object the same:

```
db.createUser( {
     user: "root-admin",
     pwd: "daphnesnohansenhyrule",
     roles: [ { role: "root", db: "admin" } ]
   });
```

Now that we are done creating users we can use for login, we are going to destroy this mongo container and rebuild. Don't worry, the user info we just built will be store in the home directory that we built earlier:

```
exit
docker stop mongo
docker rm mongo
```

## 4. KEYFILE CREATION
Now you should be back within the /home/core directory. (If not then cd /home/core to return to it). If you run ls within this folder structure, you should see a folder named mongo-files. cd into this folder, where the mongo user data we created eariler is stored, and we are going to create a keyfile to be used by mongo.

```
openssl rand -base64 741 > mongodb-keyfile
chmod 600 mongodb-keyfile
chown 999 mongodb-keyfile
```

The file owner we changed to a user id of "999" because the user in the MongoDB Docker contaienr is the one that needs access to this key file

Once you create this key file on NODE1, be sure to copy this same file to the /home/core/mongo-files directory on the other two node instances you have built. (If the other two do not have the mongo-data folder, then follow all of step three on the other tow servers. DO NOT generate a new mongo-keyfile on the other two, this will cause conflicts. Easiest way to copy is to run __cat mongo-keyfile__ on NODE1 to print out it's results, then run __vi mongo-keyfile__ on NODE2 and NODE3 to create a bnew file and paste it in. IF you are unfamiliar with VI, then simply hit __ESC + I__ to insert data, hit __CMD + V__ to paste the output from the original file, then hit __ESC__ and type __:wq__ to save the file)

## 5. REBUILDING
Now that we have keyfiles, we are ready to create the final mongo containers. On NODE1, run the following command:

```
docker run \
--name mongo \
-v /home/core/mongo-files/data:/data/db \
-v /home/core/mongo-files:/opt/keyfile \
--hostname="<NODE1_DNS_ADDRESS>" \
--add-host <NODE1_DNS_ADDRESS>:${node1} \
--add-host <NODE2_DNS_ADDRESS>:${node2} \
--add-host <NODE3_DNS_ADDRESS>:${node3} \
-p 27017:27017 -d mongo:2.6.5 \
--smallfiles \
--keyFile /opt/keyfile/mongodb-keyfile \
--replSet "rs0"
```

The "--keyFile" path is "/opt/keyfile/mongodb-keyfile". This is correct. This is the path to the keyfile inside of the docker container. We mapped the file with the "-v" option to this location inside the container.

The "--add-host" adds these entries into the Docekr container's /etc/hosts directory so we can use hostnames instaed of the IP addresses.

## 6. REPLICA INITIAL SETUP 
Now that the container is running, we need to once again open the interactive shell.

```
docker exec -it mongo /bin/bash
mongo
```

Switch to the admin user

```
use admin
```

Our user credentials from earlier are still valid, so now we will have to sign in with the credentials that you set up.

```
db.auth("siteRootAdmin", "password");
```

We can now initiate the replica set

```
rs.initiate()
```

You should see the following output on success:

```
{
         "info2" : "no configuration explicitly specified -- making one",
         "me" : "<NODE1_DNS_ADDRESS>:27017",
         "info" : "Config now saved locally.  Should come online in about a minute.",
         "ok" : 1
}
```

And to verify the initial setup for this node, run:

```
rs.conf
```
and you shpould have a similar output:

```
{
        "_id" : "rs0",
        "version" : 1,r
        "members" : [
              {
                  "_id" : 0,
                  "host" : "<NODE1_DNS_ADDRESS:27017"
              }
        ]
}
```

## 7. CREATING SERVICE WORKERS
Start the same mongo containers on the other two nodes. This will be the same docker run command we ran earlier in step 5, but on each node be sure to __change the hostname address to the current node's DNS address__

NODE2

```
docker run \
--name mongo \
-v /home/core/mongo-files/data:/data/db \
-v /home/core/mongo-files:/opt/keyfile \
--hostname="<NODE2_DNS_ADDRESS>" \
--add-host <NODE1_DNS_ADDRESS>:${node1} \
--add-host <NODE2_DNS_ADDRESS>:${node2} \
--add-host <NODE3_DNS_ADDRESS>:${node3} \
-p 27017:27017 -d mongo:2.6.5 \
--smallfiles \
--keyFile /opt/keyfile/mongodb-keyfile \
--replSet "rs0"
```

NODE3

```
docker run \
--name mongo \
-v /home/core/mongo-files/data:/data/db \
-v /home/core/mongo-files:/opt/keyfile \
--hostname="<NODE2_DNS_ADDRESS>" \
--add-host <NODE1_DNS_ADDRESS>:${node1} \
--add-host <NODE2_DNS_ADDRESS>:${node2} \
--add-host <NODE3_DNS_ADDRESS>:${node3} \
-p 27017:27017 -d mongo:2.6.5 \
--smallfiles \
--keyFile /opt/keyfile/mongodb-keyfile \
--replSet "rs0"
```

## 8. CONNECTING WORKERS TO MANAGER
Now we will add the other 2 nodes to the replica set. Back on NODE1, you should still be within the mongo shell. You should now see that you are running commands as "rs0:PRIMARY", as this node is now considered the primary node for the replica set we named "rs0". To add NODE 2 & 3, we simply run the following commnads using the DNS addresses that we exported earlier:

```
rs.add("db2.s6-branchburg-prod.mongo.verizoncloudplatform.com")
rs.add("db3.s3-coloradosprings-prod.mongo.verizoncloudplatform.com")
```

You can validate that these node have been added in correctly by running:

```
rs.status()
```

If you are getting errors with adding a node, try waiting about 1-2 minutes before trying again, as it can take a second for the primary to detect that the other nodes have a running container. If you ever need to see the logs of your replica set, simply run the following commnad when you are at the root level of your instance (not the mongo shell insdie the container)

```
docker logs -ft mongo
```

## 9. CREATING NEW DATABASES
Last thing we need to do is create a spefic database for an applications use within the replica set. From NODE1, inside the mongo shell, you can simply create a new database set by running the following command:

```
use <DATABASE_NAME>
```

You should be alerted that you have switched to your new database name, then authenticate yourself again with the root admin credentials we created earlier:

```
db.auth("siteRootAdmin", "password");
```

You should now be authenticated, but the database will not be considered created until we add some data. simply run the follwoing command to add a single table to your DB, and it will be created:

```
db.user.insert({"name":"Bob Hoskins"})
```

"user" can be what ever you wish to name the table model, then all data inside of the insert will be added. 

## 10. REPLICA MANAGEMENT
You can verify that your mongodb replica cluster is up and running properly by using a GUI tool known as [Studio 3T](https://studio3t.com/download/). Once you have this installed, simply click the __Connect__ option in the top-left corner of the application. This will show you a table of any connections you currently have saved so you can connect to them. 

To create a new connection, click on __New Connection__ in this popup table. Add a name for your connection, then click the option for __From URI...__ towards the bottom of the prompt. Enter in the __DNS ADDRESS__ of your NODE1 instance, with the mongo specified path beofre it.

Example: `mongodb://node1.example.com`

Click okay, and it will automatically attach this path to your server connection with the port of __27017, mongo's default port__. 

Next select the __Authentication__ tab, and select __BASIC__ from the __Authentication Mode__ drop down option. Here you will enter your Root Admin username and password that we created earlier.

Since we are using Openstack instances with floating IP's , we will need to add SSH proxy configuration. So now go to the __SSH Tunnel__ tab, and enter the DNS address for NODE1 within __SSH ADDRESS__. Leave the port number as __port 22__, username should be __ubuntu__, unless you have a user configured on your docker-machine instance. Change __SSH Auth Mode__ to __Private KEy__, and navigate to your ssh private key file within the __Private Key__ input. (Passpharse should not be required unless you set one up.

Finally, return to the __Server__ tab, and change __Connection type__ to __Replica Set or Shared Cluster__. Your NODE1 DNS should still be listed, then hit the __Discover__ button. This will test the connection to your nodes, and automatically find the shared replica memebers. 

Once this is done, simply Save you connection, and feel free to browse your database replica set. There are a ton of things you can manipulate using Studio 3T, so feel free to read their documentation to see all that you can do.

###Congrats, you now have a successful mongoDB Replica set cluster.
