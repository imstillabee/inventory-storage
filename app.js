import path from 'path'
import express from 'express'
import mongoose from 'mongoose'
import apiRouter from './api/routes'
import cluster from 'cluster'
import bodyParser from 'body-parser'
import swaggerUi from 'swagger-ui-express'
import swaggerDocs from './swagger/swagger.js'


const server = module.exports = express();

let config = require('./config')

if (cluster.isMaster && require.main === module) {
  const cpuCount = require('os').cpus().length

  for (let i = 0; i < cpuCount; i++)
      cluster.fork()

  cluster.on('exit', worker => {
      console.log(`Worker ${worker.id} died`)
      cluster.fork()
  })
} else {
  config.connectToDb()

  server.use(bodyParser.json({limit: '10mb'}))

  server.use('/docs', swaggerUi.serve, swaggerUi.setup(swaggerDocs))
  server.use( '/api/v1', apiRouter)

  if(require.main === module){
    server.listen(config.app.port, () => {
      if (config.env !== 'test')
        console.info(`${config.app.description} Listening on port ${config.app.port}`)
    })
  }
}
