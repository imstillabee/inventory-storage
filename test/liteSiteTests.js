import chai from 'chai'
import { expect, should, assert } from 'chai'
import site from './testData/liteSiteData'

let chaiHttp = require('chai-http')

const server = require('../app')

chai.use(chaiHttp)

describe('POST Lite_Site', () => {
  it('Should POST a Lite Site', (done) => {
    chai.request(server)
    .post('/api/v1/sites')
    .send(site)
    .end((err, res) => {
      if (res) {
        assert.equal(200, res.statusCode)
        res.body.should.be.a('object')
      } else {
          throw new Error(err)
      }
      done()
    })
  })
})
describe('GET Lite_Site', () => {
  it('Should load /sites no explosions', (done) => {
    chai.request(server)
    .get('/api/v1/sites')
    .end((err, res) => {
      if (res) {
        assert.equal(200, res.statusCode)
      } else {
        throw new Error(err)
      }
      done()
    })
  })
  it('Should load /sites with depth parameter envoked', (done) => {
    chai.request(server)
    .get('/api/v1/sites?depth=1')
    .end((err, res) => {
      if (res) {
        assert.equal(200, res.statusCode)
      } else {
        throw new Error(err)
      }
      done()
    })
  })
  it('Should return Not Found', (done) => {
    chai.request(server)
    .get('/INVALID_PATH')
    .then(res => {
      throw new Error(res, 'Path exists!')
    })
    // throws the expect 404
    .catch(err => {
      expect(err).to.have.status(404)
      done()
    })
  })
  it('Should Test content of /sites', (done) => {
    chai.request(server)
    .get('/api/v1/sites?type=lite')
    .end((err, res) => {
      if (res) {
        assert(res.should.be.json, 'body should contain json')
        assert(res.should.have.status(200), 'res status should be 200')
        assert.isArray(res.body, 'res.body should be an array')
        res.body.length.should.be.eql(1)
        res.body[0].should.be.a('object')
        res.body[0].should.have.property('name')
        res.body[0].name.should.be.a('string')
        res.body[0].name.should.be.equal('milan')
        res.body[0].should.have.property('updatedAt')
        res.body[0].updatedAt.should.be.a('string')
        res.body[0].should.have.property('type')
        res.body[0].type.should.be.a('string')
        res.body[0].type.should.be.equal('lite')
        res.body[0].should.have.property('__v')
        res.body[0].__v.should.be.a('number')
        res.body[0].should.have.property('created_at')
        res.body[0].created_at.should.be.a('string')
        // regions tests
        res.body[0].should.have.property('regions')
        res.body[0].regions.should.be.a('array')
        res.body[0].regions[0].should.be.a('object')
        res.body[0].regions[0].should.have.property('region')
        res.body[0].regions[0].region.should.be.a('string')
        res.body[0].regions[0].region.should.be.equal('r1')
        // rack tests
        res.body[0].regions[0].should.have.property('racks')
        res.body[0].regions[0].racks.should.be.a('array')
        res.body[0].regions[0].racks[0].should.be.a('object')
        res.body[0].regions[0].racks[0].should.have.property('name')
        res.body[0].regions[0].racks[0].name.should.be.a('string')
        res.body[0].regions[0].racks[0].name.should.be.equal('06X17')
        res.body[0].regions[0].racks[0].should.have.property('location')
        res.body[0].regions[0].racks[0].location.should.be.a('string')
        res.body[0].regions[0].racks[0].location.should.be.equal(res.body[0].name)
        // device tests
        res.body[0].regions[0].racks[0].devices.should.be.a('array')
        res.body[0].regions[0].racks[0].devices[0].should.be.a('object')

        if (res.body[0].regions[0].racks[0].devices[0].ipAddress) {
          res.body[0].regions[0].racks[0].devices[0].should.have.property('ipAddress')
          res.body[0].regions[0].racks[0].devices[0].macAddress.should.be.a('string')
        }

        res.body[0].regions[0].racks[0].devices[0].should.have.property('port')
        res.body[0].regions[0].racks[0].devices[0].port.should.be.a('string')
        res.body[0].regions[0].racks[0].devices[0].should.have.property('slot')
        res.body[0].regions[0].racks[0].devices[0].slot.should.be.a('string')
        res.body[0].regions[0].racks[0].devices[0].should.have.property('description')
        res.body[0].regions[0].racks[0].devices[0].description.should.be.a('string')
        res.body[0].regions[0].racks[0].devices[0].should.have.property('macAddress')
        res.body[0].regions[0].racks[0].devices[0].macAddress.should.be.a('string')
        res.body[0].regions[0].racks[0].devices[0].should.have.property('info')
      } else {
          throw new Error(err)
      }
      done()
    })
  })
  it('Should test content of /sites/:name', (done) => {
    chai.request(server)
    .get('/api/v1/sites/milan')
    .end((err, res) => {
      if (res) {
        assert(res.should.be.json, 'body should contain json')
        assert(res.should.have.status(200), 'res status should be 200')
        assert.isArray(res.body, 'res.body should be an array')
        res.body.length.should.be.eql(1)
        res.body[0].should.be.a('object')
        res.body[0].should.have.property('name')
        res.body[0].name.should.be.equal('milan')
        res.body[0].name.should.be.a('string')
        res.body[0].should.have.property('updatedAt')
        res.body[0].updatedAt.should.be.a('string')
        res.body[0].should.have.property('type')
        res.body[0].type.should.be.a('string')
        res.body[0].type.should.be.equal('lite')
        res.body[0].should.have.property('__v')
        res.body[0].__v.should.be.a('number')
        res.body[0].should.have.property('created_at')
        res.body[0].created_at.should.be.a('string')
        // regions tests
        res.body[0].should.have.property('regions')
        res.body[0].regions.should.be.a('array')
        res.body[0].regions[0].should.be.a('object')
        res.body[0].regions[0].should.have.property('region')
        res.body[0].regions[0].region.should.be.a('string')
        res.body[0].regions[0].region.should.be.equal('r1')
        // rack tests
        res.body[0].regions[0].should.have.property('racks')
        res.body[0].regions[0].racks.should.be.a('array')
        res.body[0].regions[0].racks[0].should.be.a('object')
        res.body[0].regions[0].racks[0].should.have.property('name')
        res.body[0].regions[0].racks[0].name.should.be.a('string')
        res.body[0].regions[0].racks[0].name.should.be.equal('06X17')
        res.body[0].regions[0].racks[0].should.have.property('location')
        res.body[0].regions[0].racks[0].location.should.be.a('string')
        res.body[0].regions[0].racks[0].location.should.be.equal(res.body[0].name)
        // device tests
        res.body[0].regions[0].racks[0].devices.should.be.a('array')
        res.body[0].regions[0].racks[0].devices[0].should.be.a('object')

        if (res.body[0].regions[0].racks[0].devices[0].ipAddress) {
            res.body[0].regions[0].racks[0].devices[0].should.have.property('ipAddress')
            res.body[0].regions[0].racks[0].devices[0].macAddress.should.be.a('string')
        }

        res.body[0].regions[0].racks[0].devices[0].should.have.property('port')
        res.body[0].regions[0].racks[0].devices[0].port.should.be.a('string')
        res.body[0].regions[0].racks[0].devices[0].should.have.property('slot')
        res.body[0].regions[0].racks[0].devices[0].slot.should.be.a('string')
        res.body[0].regions[0].racks[0].devices[0].should.have.property('description')
        res.body[0].regions[0].racks[0].devices[0].description.should.be.a('string')
        res.body[0].regions[0].racks[0].devices[0].should.have.property('macAddress')
        res.body[0].regions[0].racks[0].devices[0].macAddress.should.be.a('string')
        res.body[0].regions[0].racks[0].devices[0].should.have.property('info')
      } else {
          throw new Error(err)
      }
      done()
    })
  })
  it('Should test content of /sites/:name/:region', (done)=> {
    chai.request(server)
    .get('/api/v1/sites/milan/r1')
    .end((err, res) => {
      if (res) {
        assert(res.should.be.json, 'body should contain json')
        assert(res.should.have.status(200), 'res status should be 200')
        assert.isArray(res.body, 'res.body should be an array')
        res.should.be.json
        res.body.should.be.a('array')
        res.body[0].should.be.a('object')
        res.body[0].should.have.property('region')
        res.body[0].region.should.be.a('string')
        res.body[0].region.should.be.equal('r1')
        // rack tests
        res.body[0].should.have.property('racks')
        res.body[0].racks.should.be.a('array')
        res.body[0].racks[0].should.be.a('object')
        res.body[0].racks[0].should.have.property('name')
        res.body[0].racks[0].name.should.be.a('string')
        res.body[0].racks[0].name.should.be.equal('06X17')
        res.body[0].racks[0].should.have.property('location')
        res.body[0].racks[0].location.should.be.a('string')
        res.body[0].racks[0].location.should.be.equal('milan')
        // device tests
        res.body[0].racks[0].devices.should.be.a('array')
        res.body[0].racks[0].devices[0].should.be.a('object')

        if (res.body[0].racks[0].devices[0].ipAddress) {
            res.body[0].racks[0].devices[0].should.have.property('ipAddress')
            res.body[0].racks[0].devices[0].macAddress.should.be.a('string')
        }

        res.body[0].racks[0].devices[0].should.have.property('port')
        res.body[0].racks[0].devices[0].port.should.be.a('string')
        res.body[0].racks[0].devices[0].should.have.property('slot')
        res.body[0].racks[0].devices[0].slot.should.be.a('string')
        res.body[0].racks[0].devices[0].should.have.property('description')
        res.body[0].racks[0].devices[0].description.should.be.a('string')
        res.body[0].racks[0].devices[0].should.have.property('macAddress')
        res.body[0].racks[0].devices[0].macAddress.should.be.a('string')
        res.body[0].racks[0].devices[0].should.have.property('info')
      } else {
          throw new Error(err)
      }
      done()
    })
  })
  it('Should test content of /sites/:nane/:region/:rackname', (done) => {
    chai.request(server)
    .get('/api/v1/sites/milan/r1/06X17')
    .end((err, res) => {
      if (res) {
        assert(res.should.be.json, 'body should contain json')
        assert(res.should.have.status(200), 'res status should be 200')
        assert.isArray(res.body, 'res.body should be an array')
        res.body[0].should.have.property('name')
        res.body[0].name.should.be.a('string')
        res.body[0].name.should.be.equal('06X17')
        res.body[0].should.have.property('location')
        res.body[0].location.should.be.a('string')
        res.body[0].location.should.be.equal('milan')
        // device tests
        res.body[0].devices.should.be.a('array')
        res.body[0].devices[0].should.be.a('object')

        if (res.body[0].devices[0].ipAddress) {
            res.body[0].devices[0].should.have.property('ipAddress')
            res.body[0].devices[0].macAddress.should.be.a('string')
        }

        res.body[0].devices[0].should.have.property('port')
        res.body[0].devices[0].port.should.be.a('string')
        res.body[0].devices[0].should.have.property('slot')
        res.body[0].devices[0].slot.should.be.a('string')
        res.body[0].devices[0].should.have.property('description')
        res.body[0].devices[0].description.should.be.a('string')
        res.body[0].devices[0].should.have.property('macAddress')
        res.body[0].devices[0].macAddress.should.be.a('string')
        res.body[0].devices[0].should.have.property('info')
      } else {
          throw new Error(err)
      }
      done()
    })
  })
})
