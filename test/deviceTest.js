import chai from 'chai'
var chaiHttp = require('chai-http')
var server = require('../app')
const assert = require('chai').assert
import site from './testData/liteSiteData'

let expect = chai.expect;
let should = chai.should()

chai.use(chaiHttp)

describe('POST Devices', () => {
  it('should pass after RES from POST is on /devices', (done) => {
    chai.request(server)
    .post('/api/v1/devices')
    .send(site)
    .end((err, res) => {
      res.should.have.status(200)
      res.body.should.be.a('object')
      done()
    })
  })
})
describe('GET Devices', () => {
  it('should pass after RES from GET /devices', (done) => {
    chai.request(server)
    .get('/api/v1/devices')
    .end((err, res) => {
      res.should.be.json
      res.should.have.status(200)
      res.body.should.be.a('array')
      done()
    })
  })
  it('should pass after RES from, GET /devices/<macAddress> GET', (done) => {
    chai.request(server)
    .get('/api/v1/devices/:macAddress')
    .end((err, res) => {
      res.should.have.status(200)
      res.body.should.be.a('object')
      done()
    })
  })
})
