import chai from 'chai'
import { expect, should, assert } from 'chai'
import site from './testData/coreSiteData'

let chaiHttp = require('chai-http')

const server = require('../app')

chai.use(chaiHttp)

describe('POST Core-Site', () => {
  it('Should POST a Core Site', (done) => {
  chai.request(server)
  .post('/api/v1/sites')
  .send(site)
  .end((err, res) => {
    if (res) {
      assert.equal(200, res.statusCode)
      res.body.should.be.a('object')
    } else {
        throw new Error(err)
    }
    done()
    })
  })
})
describe('GET Core_Site', () => {
  it('Should load /sites no explosions', (done) => {
  chai.request(server)
  .get('/api/v1/sites')
  .end((err, res) => {
    if (res) {
      assert.equal(200, res.statusCode)
    } else {
        throw new Error(err)
    }
    done()
    })
  })
  it('Should return Not Found', (done) => {
    chai.request(server)
    .get('/INVALID_PATH')
    .then(res => {
      throw new Error(res, 'Path exists!')
    })
    // throws the expect 404
    .catch(err => {
      expect(err).to.have.status(404)
      done()
    })
  })
  it('Should test the content of /sites', (done) => {
    chai.request(server)
    .get('/api/v1/sites?type=core')
    .end((err, res) => {
      if (res) {
        let racks = res.body[0].regions[0]['racks']
        let devices = racks[0]['devices']
        const regions = res.body[0].regions

        assert(res.should.be.json, 'body should contain json')
        assert(res.should.have.status(200), 'res status should be 200')
        assert.isArray(res.body, 'res.body should be an array')
        res.body.length.should.be.eql(1)
        res.body[0].should.be.a('object')
        res.body[0].should.have.property('name')
        res.body[0].name.should.be.a('string')
        res.body[0].name.should.be.equal('arlington')
        res.body[0].should.have.property('updatedAt')
        res.body[0].updatedAt.should.be.a('string')
        res.body[0].should.have.property('type')
        res.body[0].type.should.be.a('string')
        res.body[0].type.should.be.equal('core')
        res.body[0].should.have.property('clli')
        res.body[0].should.have.property('__v')
        res.body[0].__v.should.be.a('number')
        res.body[0].should.have.property('created_at')
        res.body[0].created_at.should.be.a('string')
        res.body[0].should.have.property('regions')
        res.body[0].regions.should.be.a('array')
        res.body[0].regions.length.should.be.eql(6)

        for (let i = 0; i < regions.length; i++) {
          switch(regions[i].region) {
            case 'shared':
              regions[i].region.should.be.a('string')
              regions[i].region.should.be.equal('shared')
              let location = regions[i].racks[i].location

              if (location) {
                location.should.be.a('string')
                location.should.be.equal(res.body[0].name)
              }
              let devices = regions[i].racks[i].devices
              for (let j = 0; j < devices.length; j++) {
                  if (devices) {
                    if (devices[j].ipAddress) {
                      devices[j].should.have.property('ipAddress')
                      devices[j].ipAddress.should.be.a('string')
                    }

                    if (devices[j].port && devices[j].slot) {
                      devices[j].should.have.property('port')
                      devices[j].port.should.be.a('string')
                      devices[j].should.have.property('slot')
                      devices[j].slot.should.be.a('string')
                    }

                    devices[j].should.have.property('description')
                    devices[j].description.should.be.a('string')
                    devices[j].should.have.a.property('macAddress')
                    devices[j].macAddress.should.be.a('string')
                    devices[j].should.have.property('info')
                  } else {
                      console.log('Error: Devices: ', devices[j])
                  }
              }
            break
            case 'r1':
              regions[i].region.should.be.a('string')
              regions[i].region.should.be.equal('r1')

              if(location) {
                location.should.be.a('string')
                location.should.be.equal(res.body[0].name)
              }

              let racks = regions[i].racks

              for (let j = 0; j < racks.length; j++) {
                racks[j].should.have.property('name')
                racks[j].name.should.be.a('string')
                racks[j].should.have.property('location')
                racks[j].location.should.be.a('string')
                racks[j].location.should.be.equal(res.body[0].name)

                  devices = regions[i].racks[j].devices

                for (let x = 0; x < devices.length; x++) {
                  if (devices) {
                    if (devices[x].ipAddress) {
                      devices[x].should.have.property('ipAddress')
                      devices[x].ipAddress.should.be.a('string')
                    }

                    if (devices[x].port && devices[x].slot) {
                      devices[x].should.have.property('port')
                      devices[x].port.should.be.a('string')
                      devices[x].should.have.property('slot')
                      devices[x].slot.should.be.a('string')
                    }

                    devices[x].should.have.property('description')
                    devices[x].description.should.be.a('string')
                    devices[x].should.have.a.property('macAddress')
                    devices[x].macAddress.should.be.a('string')
                    devices[x].should.have.property('info')
                  }
                }
              }
            break
            case 'r2':
              regions[i].region.should.be.a('string')
              regions[i].region.should.be.equal('r2')
              if(location) {
                location.should.be.a('string')
                location.should.be.equal(res.body[0].name)
              }
                for (let j = 0; j < racks.length; j++) {
                  racks[j].should.have.property('name')
                  racks[j].name.should.be.a('string')
                  racks[j].should.have.property('location')
                  racks[j].location.should.be.a('string')
                  racks[j].location.should.be.equal(res.body[0].name)

                    devices = regions[i].racks[j].devices

                    for (let x = 0; x < devices.length; x++) {
                      if (devices) {
                        if (devices[x].ipAddress) {
                          devices[x].should.have.property('ipAddress')
                          devices[x].ipAddress.should.be.a('string')
                        }

                        if (devices[x].port && devices[x].slot) {
                          devices[x].should.have.property('port')
                          devices[x].port.should.be.a('string')
                          devices[x].should.have.property('slot')
                          devices[x].slot.should.be.a('string')
                        }

                        devices[x].should.have.property('description')
                        devices[x].description.should.be.a('string')
                        devices[x].should.have.a.property('macAddress')
                        devices[x].macAddress.should.be.a('string')
                        devices[x].should.have.property('info')
                      }
                    }
                }
            break
            case 'r3':
                regions[i].region.should.be.a('string')
                regions[i].region.should.be.equal('r3')

                if(location) {
                  location.should.be.a('string')
                  location.should.be.equal(res.body[0].name)
                }
                for (let j = 0; j < racks.length; j++) {
                  racks[j].should.have.property('name')
                  racks[j].name.should.be.a('string')
                  racks[j].should.have.property('location')
                  racks[j].location.should.be.a('string')
                  racks[j].location.should.be.equal(res.body[0].name)

                    devices = regions[i].racks[j].devices

                    for (let x = 0; x < devices.length; x++) {
                      if (devices) {
                        if (devices[x].ipAddress) {
                          devices[x].should.have.property('ipAddress')
                          devices[x].ipAddress.should.be.a('string')
                        }

                        if (devices[x].port && devices[x].slot) {
                          devices[x].should.have.property('port')
                          devices[x].port.should.be.a('string')
                          devices[x].should.have.property('slot')
                          devices[x].slot.should.be.a('string')
                        }

                        devices[x].should.have.property('description')
                        devices[x].description.should.be.a('string')
                        devices[x].should.have.a.property('macAddress')
                        devices[x].macAddress.should.be.a('string')
                        devices[x].should.have.property('info')
                      }
                    }
                }
            break
            case 'r4':
              regions[i].region.should.be.a('string')
              regions[i].region.should.be.equal('r4')

                if(location) {
                  location.should.be.a('string')
                  location.should.be.equal(res.body[0].name)
                }
                for (let j = 0; j < racks.length; j++) {
                  racks[j].should.have.property('name')
                  racks[j].name.should.be.a('string')
                  racks[j].should.have.property('location')
                  racks[j].location.should.be.a('string')
                  racks[j].location.should.be.equal(res.body[0].name)

                    devices = regions[i].racks[j].devices

                    for (let x = 0; x < devices.length; x++) {
                      if (devices) {
                        if (devices[x].ipAddress) {
                          devices[x].should.have.property('ipAddress')
                          devices[x].ipAddress.should.be.a('string')
                        }

                        if (devices[x].port && devices[x].slot) {
                          devices[x].should.have.property('port')
                          devices[x].port.should.be.a('string')
                          devices[x].should.have.property('slot')
                          devices[x].slot.should.be.a('string')
                        }

                        devices[x].should.have.property('description')
                        devices[x].description.should.be.a('string')
                        devices[x].should.have.a.property('macAddress')
                        devices[x].macAddress.should.be.a('string')
                        devices[x].should.have.property('info')
                      }
                    }
                }
            break
            case 'security':
              regions[i].region.should.be.a('string')
              regions[i].region.should.be.equal('security')
                if(location) {
                  location.should.be.a('string')
                  location.should.be.equal(res.body[0].name)
                }
                for (let j = 0; j < racks.length; j++) {
                  racks[j].should.have.property('name')
                  racks[j].name.should.be.a('string')
                  racks[j].should.have.property('location')
                  racks[j].location.should.be.a('string')
                  racks[j].location.should.be.equal(res.body[0].name)

                    for (let x = 0; x < devices.length; x++) {
                      if (devices) {
                        if (devices[x].ipAddress) {
                          devices[x].should.have.property('ipAddress')
                          devices[x].ipAddress.should.be.a('string')
                        }

                        if (devices[x].port && devices[x].slot) {
                          devices[x].should.have.property('port')
                          devices[x].port.should.be.a('string')
                          devices[x].should.have.property('slot')
                          devices[x].slot.should.be.a('string')
                        }

                        devices[x].should.have.property('description')
                        devices[x].description.should.be.a('string')
                        devices[x].should.have.a.property('macAddress')
                        devices[x].macAddress.should.be.a('string')
                        devices[x].should.have.property('info')
                      }
                    }
                }
             break
             default:
               console.log('I have no idea what to do with my hands')
             break
            }
         }
      }
      done()
    })
  })
  it('Should test the content of /sites/name', (done)=> {
    chai.request(server)
    .get('/api/v1/sites/arlington?type=core')
    .end((err, res) => {
      if (res) {
        const regions = res.body[0].regions
          assert(res.should.be.json, 'body should contain json')
          assert(res.should.have.status(200), 'res status should be 200')
          assert.isArray(res.body, 'res.body should be an array')
          res.body.length.should.be.eql(1)
          res.body[0].should.be.a('object')
          res.body[0].should.have.property('name')
          res.body[0].name.should.be.a('string')
          res.body[0].name.should.be.equal('arlington')
          res.body[0].should.have.property('updatedAt')
          res.body[0].updatedAt.should.be.a('string')
          res.body[0].should.have.property('type')
          res.body[0].type.should.be.a('string')
          res.body[0].type.should.be.equal('core')
          res.body[0].should.have.property('clli')
          res.body[0].should.have.property('__v')
          res.body[0].__v.should.be.a('number')
          res.body[0].should.have.property('created_at')
          res.body[0].created_at.should.be.a('string')
          res.body[0].should.have.property('regions')
          res.body[0].regions.should.be.a('array')
          res.body[0].regions.length.should.be.eql(6)

          for (let i = 0; i < regions.length; i++) {
            switch(regions[i].region) {
              case 'shared':
                regions[i].region.should.be.a('string')
                regions[i].region.should.be.equal('shared')
                let location = regions[i].racks[i].location

                if (location) {
                    location.should.be.a('string')
                    location.should.be.equal(res.body[0].name)
                }
                let devices = regions[i].racks[i].devices
                for (let j = 0; j < devices.length; j++) {
                    if (devices) {
                        if (devices[j].ipAddress) {
                            devices[j].should.have.property('ipAddress')
                            devices[j].ipAddress.should.be.a('string')
                        }

                        if (devices[j].port && devices[j].slot) {
                            devices[j].should.have.property('port')
                            devices[j].port.should.be.a('string')
                            devices[j].should.have.property('slot')
                            devices[j].slot.should.be.a('string')
                        }

                        devices[j].should.have.property('description')
                        devices[j].description.should.be.a('string')
                        devices[j].should.have.a.property('macAddress')
                        devices[j].macAddress.should.be.a('string')
                        devices[j].should.have.property('info')
                    } else {
                        console.log('Error: Devices: ', devices[j])
                    }
                }
              break
              case 'r1':
                regions[i].region.should.be.a('string')
                regions[i].region.should.be.equal('r1')

                if (location) {
                  location.should.be.a('string')
                  location.should.be.equal(res.body[0].name)
                }

                let racks = regions[i].racks

                for (let j = 0; j < racks.length; j++) {
                  racks[j].should.have.property('name')
                  racks[j].name.should.be.a('string')
                  racks[j].should.have.property('location')
                  racks[j].location.should.be.a('string')
                  racks[j].location.should.be.equal(res.body[0].name)

                  devices = regions[i].racks[j].devices

                    for (let x = 0; x < devices.length; x++) {
                      if (devices) {
                        if (devices[x].ipAddress) {
                          devices[x].should.have.property('ipAddress')
                          devices[x].ipAddress.should.be.a('string')
                        }

                        if (devices[x].port && devices[x].slot) {
                          devices[x].should.have.property('port')
                          devices[x].port.should.be.a('string')
                          devices[x].should.have.property('slot')
                          devices[x].slot.should.be.a('string')
                        }

                        devices[x].should.have.property('description')
                        devices[x].description.should.be.a('string')
                        devices[x].should.have.a.property('macAddress')
                        devices[x].macAddress.should.be.a('string')
                        devices[x].should.have.property('info')
                      }
                    }
                }
              break
              case 'r2':
                regions[i].region.should.be.a('string')
                regions[i].region.should.be.equal('r2')
                if(location) {
                  location.should.be.a('string')
                  location.should.be.equal(res.body[0].name)
                }
                for (let j = 0; j < racks.length; j++) {
                  racks[j].should.have.property('name')
                  racks[j].name.should.be.a('string')
                  racks[j].should.have.property('location')
                  racks[j].location.should.be.a('string')
                  racks[j].location.should.be.equal(res.body[0].name)

                  devices = regions[i].racks[j].devices

                    for (let x = 0; x < devices.length; x++) {
                      if (devices) {
                        if (devices[x].ipAddress) {
                          devices[x].should.have.property('ipAddress')
                          devices[x].ipAddress.should.be.a('string')
                        }

                        if (devices[x].port && devices[x].slot) {
                          devices[x].should.have.property('port')
                          devices[x].port.should.be.a('string')
                          devices[x].should.have.property('slot')
                          devices[x].slot.should.be.a('string')
                        }

                        devices[x].should.have.property('description')
                        devices[x].description.should.be.a('string')
                        devices[x].should.have.a.property('macAddress')
                        devices[x].macAddress.should.be.a('string')
                        devices[x].should.have.property('info')
                      }
                    }
                }
              break
              case 'r3':
                regions[i].region.should.be.a('string')
                regions[i].region.should.be.equal('r3')

                if(location) {
                  location.should.be.a('string')
                  location.should.be.equal(res.body[0].name)
                }
                for (let j = 0; j < racks.length; j++) {
                  racks[j].should.have.property('name')
                  racks[j].name.should.be.a('string')
                  racks[j].should.have.property('location')
                  racks[j].location.should.be.a('string')
                  racks[j].location.should.be.equal(res.body[0].name)

                  devices = regions[i].racks[j].devices

                    for (let x = 0; x < devices.length; x++) {
                      if (devices) {
                        if (devices[x].ipAddress) {
                            devices[x].should.have.property('ipAddress')
                            devices[x].ipAddress.should.be.a('string')
                        }

                        if (devices[x].port && devices[x].slot) {
                          devices[x].should.have.property('port')
                          devices[x].port.should.be.a('string')
                          devices[x].should.have.property('slot')
                          devices[x].slot.should.be.a('string')
                        }

                        devices[x].should.have.property('description')
                        devices[x].description.should.be.a('string')
                        devices[x].should.have.a.property('macAddress')
                        devices[x].macAddress.should.be.a('string')
                        devices[x].should.have.property('info')
                      }
                    }
                }
              break
              case 'r4':
                  regions[i].region.should.be.a('string')
                  regions[i].region.should.be.equal('r4')

                  if(location) {
                    location.should.be.a('string')
                    location.should.be.equal(res.body[0].name)
                  }
                  for (let j = 0; j < racks.length; j++) {
                    racks[j].should.have.property('name')
                    racks[j].name.should.be.a('string')
                    racks[j].should.have.property('location')
                    racks[j].location.should.be.a('string')
                    racks[j].location.should.be.equal(res.body[0].name)

                    devices = regions[i].racks[j].devices

                      for (let x = 0; x < devices.length; x++) {
                        if (devices) {
                          if (devices[x].ipAddress) {
                            devices[x].should.have.property('ipAddress')
                            devices[x].ipAddress.should.be.a('string')
                          }

                          if (devices[x].port && devices[x].slot) {
                            devices[x].should.have.property('port')
                            devices[x].port.should.be.a('string')
                            devices[x].should.have.property('slot')
                            devices[x].slot.should.be.a('string')
                          }

                          devices[x].should.have.property('description')
                          devices[x].description.should.be.a('string')
                          devices[x].should.have.a.property('macAddress')
                          devices[x].macAddress.should.be.a('string')
                          devices[x].should.have.property('info')
                        }
                      }
                  }
                break
                case 'security':
                    regions[i].region.should.be.a('string')
                    regions[i].region.should.be.equal('security')
                    if(location) {
                      location.should.be.a('string')
                      location.should.be.equal(res.body[0].name)
                    }
                    for (let j = 0; j < racks.length; j++) {
                      racks[j].should.have.property('name')
                      racks[j].name.should.be.a('string')
                      racks[j].should.have.property('location')
                      racks[j].location.should.be.a('string')
                      racks[j].location.should.be.equal(res.body[0].name)

                        for (let x = 0; x < devices.length; x++) {
                          if (devices) {
                            if (devices[x].ipAddress) {
                              devices[x].should.have.property('ipAddress')
                              devices[x].ipAddress.should.be.a('string')
                            }

                            if (devices[x].port && devices[x].slot) {
                              devices[x].should.have.property('port')
                              devices[x].port.should.be.a('string')
                              devices[x].should.have.property('slot')
                              devices[x].slot.should.be.a('string')
                            }

                            devices[x].should.have.property('description')
                            devices[x].description.should.be.a('string')
                            devices[x].should.have.a.property('macAddress')
                            devices[x].macAddress.should.be.a('string')
                            devices[x].should.have.property('info')
                          }
                        }
                    }
                break
                default:
                  console.log('I have no idea what to do with my hands')
               break
              }
          }
      }
      done()
    })
  })
})
// TODO: ignore the shared region tests
// TODO: Write tests by region
// TODO: write tests by rackname
