let config = require('../config')

export default {
  "swagger": "2.0",
  "info": {
    "description": "This is the formatter api layer of the live inventory collection project.",
    "version": "0.1.0",
    "title": "Inventory Formatter API",
    "contact": {
      "email": "william.johnson8@verizonwireless.com"
    }
  },
  "host": config.swaggerUrl,
  "basePath": "/api/v1",
  "tags": [
    {
      "name": "sites",
      "description": "Routes that enable mapping collected devices to sites, regions, and physical racks"
    },
    {
      "name": "devices",
      "description": "Routes for examining specific devices"
    },
    {
      "name": "csv",
      "description": "Endpoints for downloading CSV files generated from inventory data"
    }
  ],
  "schemes": [
    "https"
  ],
  "paths": {
    "/sites": {
      "get": {
        "tags": [
          "sites"
        ],
        "summary": "See all sites collected, including their regions and racks",
        "produces": [
          "application/json"
        ],
        "parameters": [
          {
            "name": "depth",
            "in": "query",
            "description": "Whether to populate collected device information",
            "required": false,
            "type": "string",
            "enum": [
              "0",
              "1"
            ],
            "default": "0"
          },
          {
            "name": "type",
            "in": "query",
            "description": "Filter sites by type: core or lite",
            "required": false,
            "type": "string",
            "enum": [
              "core",
              "lite"
            ]
          }
        ],
        "responses": {
          "200": {
            "description": "successful operation",
            "schema": {
              "type": "array",
              "items": {
                "$ref": "#/definitions/Site"
              }
            }
          }
        }
      }
    },
    "/sites/{siteName}": {
      "get": {
        "tags": [
          "sites"
        ],
        "summary": "See a specific site",
        "produces": [
          "application/json"
        ],
        "parameters": [
          {
            "name": "siteName",
            "in": "path",
            "description": "The name or CLLI code of a specific site. If name, lower case with no spaces.",
            "required": true,
            "type": "string"
          },
          {
            "name": "depth",
            "in": "query",
            "description": "Whether to populate collected device information",
            "required": false,
            "type": "string",
            "enum": [
              "0",
              "1"
            ],
            "default": "0"
          }
        ],
        "responses": {
          "200": {
            "description": "successful operation",
            "schema": {
              "type": "array",
              "items": {
                "$ref": "#/definitions/Site"
              }
            }
          }
        }
      }
    },
    "/sites/{siteName}/{region}": {
      "get": {
        "tags": [
          "sites"
        ],
        "summary": "See a region within a specific site",
        "produces": [
          "application/json"
        ],
        "parameters": [
          {
            "name": "siteName",
            "in": "path",
            "description": "The name or CLLI code of a specific site. If name, lower case with no spaces.",
            "required": true,
            "type": "string"
          },
          {
            "name": "region",
            "in": "path",
            "description": "The region - as r1, r2, r3, shared, etc.",
            "required": true,
            "type": "string"
          },
          {
            "name": "depth",
            "in": "query",
            "description": "Whether to populate collected device information",
            "required": false,
            "type": "string",
            "enum": [
              "0",
              "1"
            ],
            "default": "0"
          }
        ],
        "responses": {
          "200": {
            "description": "successful operation",
            "schema": {
              "type": "array",
              "items": {
                "$ref": "#/definitions/Region"
              }
            }
          },
          "404": {
            "description": "no site/region found"
          }
        }
      }
    },
    "/sites/{siteName}/{region}/{rackName}": {
      "get": {
        "tags": [
          "sites"
        ],
        "summary": "See a specific rack",
        "produces": [
          "application/json"
        ],
        "parameters": [
          {
            "name": "siteName",
            "in": "path",
            "description": "The name or CLLI code of a specific site. If name, lower case with no spaces.",
            "required": true,
            "type": "string"
          },
          {
            "name": "region",
            "in": "path",
            "description": "The region - as r1, r2, r3, shared, etc.",
            "required": true,
            "type": "string"
          },
          {
            "name": "rackName",
            "in": "path",
            "description": "The rack's name - X77, 07K99, etc",
            "required": true,
            "type": "string"
          },
          {
            "name": "depth",
            "in": "query",
            "description": "Whether to populate collected device information",
            "required": false,
            "type": "string",
            "enum": [
              "0",
              "1"
            ],
            "default": "0"
          }
        ],
        "responses": {
          "200": {
            "description": "successful operation",
            "schema": {
              "type": "array",
              "items": {
                "$ref": "#/definitions/Rack"
              }
            }
          },
          "404": {
            "description": "no site/region found"
          }
        }
      }
    },
    "/devices": {
      "get": {
        "tags": [
          "devices"
        ],
        "summary": "See all collected devices. Optionally filter by site.",
        "produces": [
          "application/json"
        ],
        "parameters": [
          {
            "name": "site",
            "in": "query",
            "description": "Filter devices by site",
            "required": false,
            "type": "string"
          }
        ],
        "responses": {
          "200": {
            "description": "successful operation",
            "schema": {
              "type": "array",
              "items": {
                "$ref": "#/definitions/Device"
              }
            }
          }
        }
      }
    },
    "/devices/{macAddress}": {
      "get": {
        "tags": [
          "devices"
        ],
        "summary": "Lookup a specific device by its mac address.",
        "produces": [
          "application/json"
        ],
        "parameters": [
          {
            "name": "macAddress",
            "in": "path",
            "description": "Mac address of the device to retrieve. Formatted as a1b2-c3d4-e5f6.",
            "required": true,
            "type": "string"
          }
        ],
        "responses": {
          "200": {
            "description": "successful operation",
            "schema": {
              "type": "array",
              "items": {
                "$ref": "#/definitions/Device"
              }
            }
          }
        }
      }
    },
    "/csv": {
      "get": {
        "tags": [
          "csv"
        ],
        "summary": "See the possible csv documents to download",
        "produces": [
          "application/json"
        ],
        "responses": {
          "200": {
            "description": "successful operation",
            "schema": {
              "type": "array",
              "items": {
                "$ref": "#/definitions/Csv"
              }
            }
          }
        }
      }
    },
    "/csv/{type}": {
      "get": {
        "tags": [
          "csv"
        ],
        "summary": "Download a particular type of csv",
        "produces": [
          "application/force-download"
        ],
        "parameters": [
          {
            "name": "type",
            "in": "path",
            "description": "The name of a type of CSV",
            "required": true,
            "type": "string",
            "enum": [
              "svc-tag",
              "inventory",
              "dell",
              "leaf-spine"
            ]
          }
        ],
        "responses": {
          "200": {
            "description": "successful operation",
            "schema": {
              "type": "file"
            }
          }
        }
      }
    },
    "/csv/{type}/{siteName}": {
      "get": {
        "tags": [
          "csv"
        ],
        "summary": "Download a particular type of csv for a given site.",
        "produces": [
          "application/force-download"
        ],
        "parameters": [
          {
            "name": "type",
            "in": "path",
            "description": "The name of a type of CSV",
            "required": true,
            "type": "string",
            "enum": [
              "svc-tag",
              "inventory",
              "dell",
              "leaf-spine"
            ]
          },
          {
            "name": "siteName",
            "in": "path",
            "description": "The name or CLLI code of a specific site. If name, lower case with no spaces.",
            "required": true,
            "type": "string"
          }
        ],
        "responses": {
          "200": {
            "description": "successful operation",
            "schema": {
              "type": "file"
            }
          },
          "404": {
            "description": "no site found"
          }
        }
      }
    },
    "/csv/{type}/{siteName}/{region}": {
      "get": {
        "tags": [
          "csv"
        ],
        "summary": "Download a particular type of csv for a particular region at a given site.",
        "produces": [
          "application/force-download"
        ],
        "parameters": [
          {
            "name": "type",
            "in": "path",
            "description": "The name of a type of CSV",
            "required": true,
            "type": "string",
            "enum": [
              "svc-tag",
              "inventory",
              "dell",
              "leaf-spine"
            ]
          },
          {
            "name": "siteName",
            "in": "path",
            "description": "The name or CLLI code of a specific site. If name, lower case with no spaces.",
            "required": true,
            "type": "string"
          },
          {
            "name": "region",
            "in": "path",
            "description": "The region - as r1, r2, r3, shared, etc.",
            "required": true,
            "type": "string"
          }
        ],
        "responses": {
          "200": {
            "description": "successful operation",
            "schema": {
              "type": "file"
            }
          },
          "404": {
            "description": "no site/region found"
          }
        }
      }
    },
    "/csv/{type}/{siteName}/{region}/{rackName}": {
      "get": {
        "tags": [
          "csv"
        ],
        "summary": "Download a particular type of csv for a particular region at a given site.",
        "produces": [
          "application/force-download"
        ],
        "parameters": [
          {
            "name": "type",
            "in": "path",
            "description": "The name of a type of CSV",
            "required": true,
            "type": "string",
            "enum": [
              "svc-tag",
              "inventory",
              "dell",
              "leaf-spine"
            ]
          },
          {
            "name": "siteName",
            "in": "path",
            "description": "The name or CLLI code of a specific site. If name, lower case with no spaces.",
            "required": true,
            "type": "string"
          },
          {
            "name": "region",
            "in": "path",
            "description": "The region - as r1, r2, r3, shared, etc.",
            "required": true,
            "type": "string"
          },
          {
            "name": "rackName",
            "in": "path",
            "description": "The rack's name - X77, 07K99, etc",
            "required": true,
            "type": "string"
          }
        ],
        "responses": {
          "200": {
            "description": "successful operation",
            "schema": {
              "type": "file"
            }
          },
          "404": {
            "description": "no site/region/rack found"
          }
        }
      }
    },
  },
  "definitions": {
    "Device": {
      "type": "object",
      "required": [
        "macAddress"
      ],
      "properties": {
        "macAddress": {
          "type": "string"
        },
        "site": {
          "type": "string"
        },
        "collector": {
          "type": "string"
        },
        "inventoryInfo": {
          "type": "object"
        }
      }
    },
    "Site": {
      "type": "object",
      "required": [
        "name"
      ],
      "properties": {
        "name": {
          "type": "string"
        },
        "type": {
          "type": "string"
        },
        "clli": {
          "type": "string"
        },
        "regions": {
          "type": "array",
          "items": {
            "$ref": "#/definitions/Region"
          }
        }
      }
    },
    "Region": {
      "type": "object",
      "required": [
        "region"
      ],
      "properties": {
        "region": {
          "type": "string"
        },
        "racks": {
          "type": "array",
          "items": {
            "$ref": "#/definitions/Rack"
          }
        }
      }
    },
    "Rack": {
      "type": "object",
      "properties": {
        "name": {
          "type": "string"
        },
        "location": {
          "type": "string"
        },
        "devices": {
          "type": "array",
          "items": {
            "$ref": "#/definitions/Item"
          }
        }
      }
    },
    "Item": {
      "type": "object",
      "required": [
        "macAddress"
      ],
      "properties": {
        "macAddress": {
          "type": "string"
        },
        "port": {
          "type": "string"
        },
        "slot": {
          "type": "string"
        },
        "description": {
          "type": "string"
        },
        "info": {
          "type": "object"
        }
      }
    },
    "Csv": {
      "type": "object",
      "properties": {
        "title": {
          "type": "string"
        },
        "headers": {
          "type": "array"
        },
        "endpoints": {
          "type": "object"
        }
      }
    }
  }
}
