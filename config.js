let mergeDeep = require('merge-deep')
let dockerSecreto = require('docker-secreto')

let mongoose = require('mongoose')

mongoose.Promise = global.Promise

const environments = {
        'default': {
          app: {
            name: 'Inventory Store',
            description: 'Project description copy',
            port: 3000
          },
          mongo: {
            options: {
              useMongoClient: true,
              reconnectTries: Number.MAX_VALUE
            }
          }
        },
        local: {
          env: 'local',
          mongo: {
            uri: 'mongodb://mongo/inventory_store'
          }
        },
        test: {
          env: 'test',
          mongo: {
            uri: 'mongodb://localhost/inventory-test'
          }
        },
        development: {
          env: 'development',
          mongo: {
            uri: 'mongodb://10.75.169.155:27017/inventory_store?readPreference=primary',
            options: {
              user: 'inv-admin',
              pass: dockerSecreto('inv_service_dev', 'BAD-PASSWORD'),
              auth: {
                authdb: 'admin'
              }
            }
          },
          apiUrl: 'https://dev.moss.verizoncloudplatform.com/inventory/v1',
          swaggerUrl: 'https://10.75.169.158'
        },
        staging: {
          env: 'staging',
          mongo: {
            uri: 'mongodb://db1.s3-southlake.mongo.verizoncloudplatform.com:27017,db2.s6-branchburg.mongo.verizoncloudplatform.com:27017,db3.s3-coloradosprings.mongo.verizoncloudplatform.com:27017/inventory_store?replicaSet=rs0&readPreference=primary',
            options: {
              user: 'root-admin',
              pass: dockerSecreto('inv_service_stage', 'BAD-PASSWORD'),
              auth: {
                authdb: 'admin'
              }
            }
          },
          apiUrl: 'https://stage.moss.verizoncloudplatform.com/inventory/v1',
          swaggerUrl: 'https://10.75.169.142'
        },
        production: {
          env: 'production',
          mongo: {
            uri: 'mongodb://db1.s3-southlake-prod.mongo.verizoncloudplatform.com:27017,db2.s6-branchburg-prod.mongo.verizoncloudplatform.com:27017,db3.s3-coloradosprings-prod.mongo.verizoncloudplatform.com:27017/inventory_store?replicaSet=rs0&readPreference=primary',
            options: {
              user: 'root-admin',
              pass: dockerSecreto('inv_service_prod', 'BAD-PASSWORD'),
              auth: {
                authdb: 'admin'
              }
            }
          }
        },
        apiUrl: 'https://moss.verizoncloudplatform.com/inventory/v1',
        swaggerUrl: 'https://10.75.169.151'
      },
      env = process.env.NODE_ENV || 'local',
      config = mergeDeep(environments.default, environments[env])

config.connectToDb = () => {
  console.info('Connecting to Mongo DB:', config.mongo.uri)
  console.info('  pass:', config.mongo.options.pass)
  const mongoConnection = mongoose.connect(config.mongo.uri, config.mongo.options)

   mongoConnection
   .then(() => {
    console.log('Inventory Store Mongo Connection: Success')
   })
   .catch(err => {
     console.log('Inventory Store Mongo Connection: Error', err)
   })
}


module.exports = config
